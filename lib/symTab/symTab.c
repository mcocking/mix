/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
28feb23 m_c  fix compiler warning
14jul20 m_c  initial version
*/

/*
 * Description: Provides a simple library of routines to implement symbol tables. Each table
 * entry is a symbol (string) value (numeric) pair.
 *
 */


#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "../../include/symTab.h"


/***************************************************************************************************
*
* symHash - create a hash value for a symbol
*
*/

static uint32_t symHash     // hash value
(
    char *sym               // input symbol
)
{
    uint32_t hash = 0;
    uint32_t c = *sym++;;

    while (*sym != '\0')
    {
        hash = (hash * 9) ^ c;
        c = *sym++;
    }

    return hash;
}


/***************************************************************************************************
*
* symTabInit - initialize a symbol table
*
* Record the size of the table and allocate the memory for the entries.
*
*/

uint32_t symTabInit         // the number of entries allocared
(
    symTab_t    *table,     // table to initialize
    uint32_t    size        // number of entries to allocate
)
{
    table->entries = malloc(size * sizeof(symTabEntry_t));
    if (table->entries != NULL)
    {
        table->size = size;

        for (int32_t i = 0; i < size; ++i)
        {
            table->entries[i].name = NULL;
            table->entries[i].value = 0;
        }
    }
    else
    {
        table->size = 0;
    }
    return size;
}

/***************************************************************************************************
*
* symTabFree - free a symbol table
*
* Free all the entries plus the string dups for all names.
*
*/

void symTabFree
(
    symTab_t    *table      // table to be freed
)
{
    uint32_t    size = table->size;

    for (int32_t i = 0; i < size; ++i)
    {
        free(table->entries[i].name);
    }
    free(table->entries);
}

/***************************************************************************************************
*
* lookUp - look up an entry by name
*
*/

symTabEntry_t *lookUp           // entry containing name, NULL if not found
(
    char            *sym,       // name to look up
    symTab_t        *table      // symbol table to search
)
{
    symTabEntry_t   *entries = table->entries;
    uint32_t        size = table->size;
    symTabEntry_t   *current = &(entries[symHash(sym) % size]);
    int32_t         count = size;
    symTabEntry_t * result = NULL;

    while (--count >= 0)
    {
        if (current->name == NULL)
        {
            break;
        }
        else if (strcmp(current->name, sym) == 0)
        {                                                   // found entry
            result = current;
            break;
        }

        if (++current >= entries + size) current = entries; // try next entry
    }

    return result;
}

/***************************************************************************************************
*
* lookUpByValue - look up an entry by value
*
*/

symTabEntry_t *lookUpByValue
(
    uint32_t        value,      // value to look up
    symTab_t        *table      // symbol table to search
)
{
    symTabEntry_t   *entries = table->entries;
    uint32_t        size = table->size;
    symTabEntry_t * result = NULL;

    // there's no fast way to look up via the value so run the table

    for (uint32_t i = 0; i < size; ++i)
    {
        if (entries[i].value == value)
        {
            result = &(entries[i]);
            break;
        }
    }

    return result;
}

/***************************************************************************************************
*
* lookUpNextPartial - look up an entry by partial match
*
* The search symbol can match the leading characters in an entry name. The search can start at
* any specified entry. The next entry at which to continue searching is also returned to the
* caller. If the last entry in the table matches then start is set to 0.
*
*/

symTabEntry_t *lookUpNextPartial    // table entry with the first partial match, NULL if no match
(
    char        *partial,           // string to match
    uint32_t    *start,             // entry index at which to start searching
                                    // output: index at which to continue searching
    symTab_t    *table              // symbol table to search
)
{
    symTabEntry_t *entries = table->entries;
    symTabEntry_t *entry = NULL;

    for (uint32_t i = *start; i < table->size; ++i)
    {
        if ((entries[i].name) != NULL)
        {
            char *entryName = entries[i].name;
            if (strncmp(entryName, partial, strlen(partial)) == 0)
            {
                entry = &(entries[i]);
                *start = (i + 1) % table->size; // continue at index 0 if last entry matches
                break;
            }
        }
    }

    return entry;
}

/***************************************************************************************************
*
* addEntry - add a new name/value pair to a table
*
*/

symTabEntry_t *addEntry         // new table entry, NULL if failed to add
(
    char            *sym,       // new name
    int64_t         val,        // new value
    symTab_t        *table      // table to which to add the new name/value pair
)
{
    symTabEntry_t   *entries = table->entries;
    uint32_t        size = table->size;
    symTabEntry_t   *current = &(entries[symHash(sym) % size]);
    int32_t         count = size;
    symTabEntry_t   *result = NULL;

    while (--count >= 0)
    {
        if (current->name == NULL)
        {                                                   // new entry
            current->name = strdup(sym);
            current->value = val;
            result = current;
            break;
        }

        if (++current >= entries + size) current = entries; // try next entry
    }

    return result;
}

/***************************************************************************************************
*
* deleteEntry - delete a name/value pair from a table
*
*/

void deleteEntry
(
    char            *sym,       // name to delete
    symTab_t        *table      // table from which to delete the name/value pair
)
{
    symTabEntry_t   *entry = lookUp(sym, table);

    if (entry != NULL)
    {
        free(entry->name);
        entry->name = NULL;
        entry->value = 0;
    }
}

/***************************************************************************************************
*
* printTable - print the name/value pairs from a table
*
* All the name/value pairs in a table are printed to stdout. Each pair is printed out on a
* separate line as <name> = <value>. A title and line prefix are also printed.
*
*/

void printTable
(
    char        *title,         // symbol table title
    char        *linePrefix,    // per line prefix
    symTab_t    *table          // table to print out
)
{
    symTabEntry_t   *entries = table->entries;
    uint32_t        size = table->size;

    printf("\n%s\n\n", title);

    for (uint32_t i = 0; i < size; ++i)
    {
        if (entries[i].name != NULL)
        {
            printf("%s%s = %lld\n", linePrefix, entries[i].name, entries[i].value);
        }
    }
}
