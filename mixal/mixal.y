/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
05mar23 m_c  fix line and line_list rules
04mar23 m_c  change comment character to '#'
14jul20 m_c  initial version
*/


/* 
 * Description: MIXAL parser
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */


%{

#include <stdio.h>

#include "assembler.h"
#include "../include/symTab.h"

int yylex(void);

%}

%code requires
{
    struct cF
    {
        unsigned long int   c;
        unsigned long int   f;
    };
}

%union
{
    struct ast          *a;
    struct cF           code;
    unsigned long int   num;
    char                *sym;
}

%token <code>   CODE
%token <num>    DIRECTIVE
%token <num>    NUMBER
%token <sym>    SYMBOL
%token <sym>    ALFSTRING
%token <sym>    COMMENT
%token          EOL

%type <a>       line
%type <a>       op
%type <a>       directive
%type <a>       literal
%type <a>       w_value
%type <a>       a_part
%type <a>       i_part
%type <a>       f_part
%type <a>       exp
%type <a>       atomic_exp
%%

line_list: /* nothing */
  | line_list line        {enqueue($2, &lineListTail);}
  ;

line:
    op                EOL {$$ = newMixLine(OP_LINE_NODE, NULL, $1, NULL);}
  | SYMBOL op         EOL {$$ = newMixLine(OP_LINE_NODE, $1, $2, NULL);}
  | directive         EOL {$$ = newMixLine(DIR_LINE_NODE, NULL, NULL, $1);}
  | SYMBOL directive  EOL {$$ = newMixLine(DIR_LINE_NODE, $1, NULL, $2);}
  | COMMENT           EOL {$$ = newMixLine(COMMENT_LINE_NODE, NULL, NULL, NULL);}
  ;

op:
    CODE {$$ = newMixOp(OP_NODE, $<code.c>1, $<code.f>1, NULL, NULL, NULL, NULL);}

  | CODE literal {$$ = newMixOp(OP_NODE, $<code.c>1, $<code.f>1, NULL, NULL, NULL, $2);}

  | CODE a_part {$$ = newMixOp(OP_NODE, $<code.c>1, $<code.f>1, $2, NULL, NULL, NULL);}
  | CODE i_part {$$ = newMixOp(OP_NODE, $<code.c>1, $<code.f>1, NULL, $2, NULL, NULL);}
  | CODE f_part {$$ = newMixOp(OP_NODE, $<code.c>1, $<code.f>1, NULL, NULL, $2, NULL);}

  | CODE a_part i_part  {$$ = newMixOp(OP_NODE, $<code.c>1, $<code.f>1, $2, $3, NULL, NULL);}
  | CODE a_part f_part  {$$ = newMixOp(OP_NODE, $<code.c>1, $<code.f>1, $2, NULL, $3, NULL);}
  | CODE i_part f_part  {$$ = newMixOp(OP_NODE, $<code.c>1, $<code.f>1, NULL, $2, $3, NULL);}

  | CODE a_part i_part f_part {$$ = newMixOp(OP_NODE, $<code.c>1, $<code.f>1, $2, $3, $4, NULL);}
  ;

directive:
    DIRECTIVE w_value {$$ = newMixDir($1, $2, NULL);}
  | DIRECTIVE ALFSTRING {$$ = newMixDir($1, NULL, $2);}
  ;

literal:
    '=' w_value '=' {$$ = newMixLiteral(LITERAL_NODE, $2);}
  ;

w_value:
    a_part                      {$$ = newMixWValue(W_NODE, $1, NULL, NULL);}
  | a_part f_part               {$$ = newMixWValue(W_NODE, $1, $2, NULL);}
  | w_value ',' a_part          {$$ = newMixWValue(W_NODE, $3, NULL, $1);}
  | w_value ',' a_part f_part   {$$ = newMixWValue(W_NODE, $3, $4, $1);}
  ;

a_part:
    exp {$$ = newMixAif(A_NODE, $1);}
  ;

i_part:
    ',' exp {$$ = newMixAif(I_NODE, $2);}
  ;

f_part:
    '(' exp ')' {$$ = newMixAif(F_NODE, $2);}
  ;

exp:
            atomic_exp {$$ = newMixExp(UNARY_EXP_NODE, NULL, '+', $1);}
  |     '+' atomic_exp {$$ = newMixExp(UNARY_EXP_NODE, NULL, '+', $2);}
  |     '-' atomic_exp {$$ = newMixExp(UNARY_EXP_NODE, NULL, '-', $2);}
  | exp '+' atomic_exp {$$ = newMixExp(BINARY_EXP_NODE, $1, '+', $3);}
  | exp '-' atomic_exp {$$ = newMixExp(BINARY_EXP_NODE, $1, '-', $3);}
  | exp '*' atomic_exp {$$ = newMixExp(BINARY_EXP_NODE, $1, '*', $3);}
  | exp '/' atomic_exp {$$ = newMixExp(BINARY_EXP_NODE, $1, '/', $3);}
  | exp '%' atomic_exp {$$ = newMixExp(BINARY_EXP_NODE, $1, '%', $3);}
  | exp ':' atomic_exp {$$ = newMixExp(BINARY_EXP_NODE, $1, ':', $3);}
  ;

atomic_exp:
    NUMBER  {$$ = newMixAtomic(NUMBER_NODE, $1, NULL);}
  | SYMBOL  {$$ = newMixAtomic(SYMBOL_NODE, 0, $1);}
  | '*'     {$$ = newMixAtomic(STAR_NODE, 0, NULL);}
  ;

%%

/***************************************************************************************************
*
* main - parse MIX source code
*
* Produce a file suitable for running on the MIX simulator. The output file is mixed source
* including the original source code interleaved with the assembled simulator commands. A
* symbol table is added to the end of the output file.
*
*/

int main(int argc, char **argv)
{
    int result = 0;
    FILE *f;

    if (argc > 1)
    {
        f = fopen(argv[1], "r");
        if (f != NULL)
        {
            symTabInit(&table, NHASH);      // initialize the symbol table

            yyrestart(f);                   // point the parser to the begining of the input file

            yyparse();                      // parse the input file

            /* 
             * Splice in the literals.
             * Fix up the literal and end labels to account for the literals added just before
             * the END directive.
             */

            spliceAndFixup();

            // run the list of lines and emit assembled commands

            listEntry_t *currentLine = lineList.next;   // skip the dummy line head
            while (currentLine != NULL)
            {
                emitCommand(currentLine->ast);
                currentLine = currentLine->next;
            }
            fclose(f);

            printTable("# SYMBOL TABLE", "# ", &table); // add the symbol table
        }
        else
        {
            fprintf(stderr, "\nerror: could not open %s\n\n", argv[1]);
            result = 1;
        }
    }
    else
    {
        fprintf(stderr, "\nerror: must specify a mixal input file\n\n");
        result = 1;
    }

    // free the line list and the abstract syntax tree

    freeAllAstNodes();
    symTabFree(&table);
    return result;
}

/***************************************************************************************************
*
* yyerror - report syntax errors by line number
*
*/

void yyerror
(
    char *s, ...        // error mesage
)
{
    fprintf(stderr, "%d: %s\n", yylineno, s);
}

