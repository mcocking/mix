/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
04mar23 m_c  include stdio.h
14jul20 m_c  initial version
*/


#ifndef ASSEMBLER_H
#define ASSEMBLER_H

#include <stdio.h>
#include <stdint.h>

#include "../include/symTab.h"

// number of entries in the symbol table

#define NHASH               9997

// local labels are of the form [0-9][Hh]

#define LOCAL_LABEL_SZ 2

// a reasonable size for a temporary string

#define TMP_CHAR_BUF_SZ     256

// directive pseudo-opcodes, arbitrarily assigned for use by scanner/parser

#define FIRST_DIRECTIVE     64

#define ALF     (FIRST_DIRECTIVE + 0)
#define CON     (FIRST_DIRECTIVE + 1)
#define END     (FIRST_DIRECTIVE + 2)
#define EQU     (FIRST_DIRECTIVE + 3)
#define ORIG    (FIRST_DIRECTIVE + 4)


#define BITS_PER_BYTE           6
#define BITS_PER_NIBBLE         3
#define BYTE_MASK               077 // note octal notation
#define MIN_L_R                 0
#define MAX_L_R                 5
#define R_MASK                  07  // F = 8L + R (note octal notation)
#define L_MASK                  (R_MASK << BITS_PER_NIBBLE)
#define DEFAULT_F               5   // default value for F part for most MIX operations
#define LEFT_FIELD_MULTIPLIER   8   // used to create a single value from l and r fields of F part

#define CHARS_PER_MIX_STR   5

typedef enum status
{
    OK,
    ERROR
} status_t;

typedef enum astNodeType
{
    STAR_NODE,
    NUMBER_NODE,
    SYMBOL_NODE,
    BINARY_EXP_NODE,
    UNARY_EXP_NODE,
    A_NODE,
    I_NODE,
    F_NODE,
    W_NODE,
    LITERAL_NODE,
    OP_NODE,
    ALF_DIR_NODE,
    CON_DIR_NODE,
    END_DIR_NODE,
    EQU_DIR_NODE,
    ORIG_DIR_NODE,
    OP_LINE_NODE,
    DIR_LINE_NODE,
    COMMENT_LINE_NODE
} astNode_t;

// abstract syntax tree

struct ast
{
    astNode_t   type;
    union subNode
    {
        struct mixAtomic
        {
            uint32_t    star;
            uint32_t    number;
            char        *symbol;
        } atomic;

        struct mixExp
        {
            char        expOp;
            struct ast  *left;
            struct ast  *right;
        } exp;

        struct mixAif
        {
            struct ast  *exp;
        } aif;

        struct mixWValue
        {
            struct ast  *exp;
            struct ast  *fPart;
            struct ast  *wValue;
        } wValue;

        struct mixLiteral
        {
            struct ast  *wValue;
        } literal;

        struct mixOp
        {
            uint8_t     c;
            uint8_t     dF;     // use this value if f part is vacuous
            struct ast  *aPart;
            struct ast  *iPart;
            struct ast  *fPart;
            struct ast  *wValue;
        } op;

        struct mixDir
        {
            char        *mixString;
            struct ast  *wValue;
        } dir;

        struct mixLine
        {
            char        *srcLine;
            struct ast  *op;
            struct ast  *dir;
            uint32_t    star;
        } line;
    } sub;
};

typedef struct ast ast_t;

typedef struct listEntry
{
    struct listEntry    *previous;
    struct listEntry    *next;
    struct ast           *ast;
} listEntry_t;


extern int              yylineno;

extern char             srcLine[TMP_CHAR_BUF_SZ];
extern char             *srcLinePtr;

extern listEntry_t      lineList;
extern listEntry_t      *lineListTail;

extern listEntry_t      literalList;
extern listEntry_t      *literalListTail;

extern uint32_t         star;

extern symTab_t         table;

extern char             asciiMixMap[];

extern ast_t    *newMixAtomic   (astNode_t, uint32_t, char *);
extern ast_t    *newMixExp      (astNode_t, ast_t *, char, ast_t *);
extern ast_t    *newMixAif      (astNode_t, ast_t *);
extern ast_t    *newMixWValue   (astNode_t, ast_t *, ast_t *, ast_t *);
extern ast_t    *newMixLiteral  (astNode_t, ast_t *);
extern ast_t    *newMixOp       (astNode_t, uint8_t, uint8_t, ast_t *, ast_t *, ast_t *, ast_t *);
extern ast_t    *newMixDir      (uint32_t, ast_t *, char *);
extern ast_t    *newMixLine     (astNode_t, char *, ast_t *, ast_t *);
extern void     freeAst         (ast_t *);
extern int32_t  evalAst         (ast_t *);

extern void     enqueue         (ast_t *, listEntry_t **);
extern void     formatCommand   (char, int32_t, char *);
extern int32_t  makeWValue      (int32_t, int32_t, uint32_t);
extern void     recordSymbol    (char *, ast_t *);
extern void     emitCommand     (ast_t *);
extern void     genSymbolName   (char *);
extern ast_t    *generateAPart  (char **);
extern void     createNewCon    (char *, ast_t *);
extern void     spliceAndFixup  (void);
extern void     freeAllAstNodes (void);

extern void     yyerror         (char *s, ...);
extern void     yyrestart       (FILE *);

#endif // ASSEMBLER_H
