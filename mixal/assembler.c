/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
05mar23 m_c  fixed seg fault for unknown symbols and cosmetic changes
04mar23 m_c  extend character codes to include '%' and '#'
04mar23 m_c  change comment character to '#'
28feb23 m_c  terminate partial symbol string in lookUpSymbol()
14jul20 m_c  initial version
*/


/* 
 * Description: MIXAL assembler
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 * Insted of producing binary output, the assembler produces a file of simulator commands used to
 * load a word into memory; set the memory pointer for loading words; set the program counter. The
 * actual word values are human readable as octal formatted ASCII strings. Octal is used because
 * the byte size of the simulator is 6 bits. The original source code is interleaved with the
 * simulator commands and a symbol table is appended to the end of the file.This makes the output
 * file useful for following the program execution while single stepping the simulator.
 *
 */


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
#include <stdint.h>
#include <stdbool.h>

#include "assembler.h"
#include "../include/symTab.h"

static symTabEntry_t *lookUpSymbol(char *symbol, uint32_t star);

char srcLine[TMP_CHAR_BUF_SZ] = "";             // copy of the source line being assembled
char *srcLinePtr = srcLine;

listEntry_t lineList = {NULL, NULL, NULL};      // list of ASTs, one per line of assembly
listEntry_t *lineListTail = &lineList;

listEntry_t literalList = {NULL, NULL, NULL};   // list of literal ASTs to be added to lineList
listEntry_t *literalListTail = &literalList;

uint32_t star = 0;      // current line - usual interpretation of '*' in assembly languages

// symbol table; populated when building AST

symTab_t         table;

char asciiMixMap[] = {
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 // ascii 0-15
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 // ascii 16-31
0, 0, 0, 57, 49, 56, 0, 55, 42, 43, 46, 44, 41, 45, 40, 47,      // ascii 32-47
30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 54, 53, 50, 48, 51, 0,  // ascii 48-63
52, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16,          // ascii 64-79
17, 18, 19, 22, 23, 24, 25, 26, 27, 28, 29, 0, 0, 0, 0, 0,      // ascii 80-95
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 // ascii 96-111
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 // ascii 112-127
};

/***************************************************************************************************
*
* enqueue - add a node to the tail of the specified AST list
*
*/

void enqueue
(
    ast_t       *ast,       // abstract syntax tree node to be added
    listEntry_t **tail      // tail of the list
)
{
    listEntry_t *newListEntry;

    newListEntry = (listEntry_t *) malloc(sizeof(listEntry_t));
    newListEntry->previous = *tail;
    newListEntry->next = NULL;
    newListEntry->ast = ast;
    (*tail)->next = newListEntry;
    *tail = newListEntry;
}

/***************************************************************************************************
*
* newMixAtomic - allocate an AST node for a MIXAL atomic
*
* An atomic of type star, number or symbol is allocated. The star is always included no matter
* the type.
*
*/

ast_t *newMixAtomic         // the newly allocated AST node
(
    astNode_t   type,       // node type
    uint32_t    number,     // numeric value of the atomic, ignored if node type is symbol or star
    char        *symbol     // symbol string, ignored if type is numeric or star
)
{
    ast_t *a = (malloc(sizeof(ast_t)));

    if (a != NULL)
    {
        a->type = type;
        a->sub.atomic.star = star;
        a->sub.atomic.symbol = symbol;
        a->sub.atomic.number = number;
    }
    return a;
}

/***************************************************************************************************
*
* newMixExp - allocate an AST node for a MIXAL expression
*
* An expression of type unary or binary is allocated.
*
*/

ast_t *newMixExp            // the newly allocated AST node
(
    astNode_t   type,       // node type
    ast_t       *left,      // left side of the expression, ignored if unary, may be NULL
    char        expOp,      // '+' or '-' if unary, '+', '-', '*', '/', '%' or ':' if binary
    ast_t       *right      // right side of the expression
)
{
    ast_t *a = (malloc(sizeof(ast_t)));

    if (a != NULL)
    {
        a->type = type;

        assert ((type == UNARY_EXP_NODE) || (type == BINARY_EXP_NODE));

        if (type == UNARY_EXP_NODE)
        {
            a->sub.exp.left = NULL;
            a->sub.exp.expOp= expOp;
            a->sub.exp.right = right;
        }
        else
        {
            a->sub.exp.left = left;
            a->sub.exp.expOp= expOp;
            a->sub.exp.right = right;
        }
    }
    return a;
}

/***************************************************************************************************
*
* newMixAif - allocate an AST node for a MIXAL A part, I part or F part
*
* A, I, and F parts are expressions.
*
*/

ast_t *newMixAif            // the newly allocated AST node
(
    astNode_t   type,       // node type
    ast_t       *exp        // expression for the A, I, or F part
)
{
    ast_t *a = (malloc(sizeof(ast_t)));

    if (a != NULL)
    {
        a->type = type;
        a->sub.aif.exp = exp;
    }
    return a;
}

/***************************************************************************************************
*
* newMixWValue - allocate an AST node for a MIXAL W value
*
* The W value is recursively defined.
* See "The Art of Computer Programming, 2nd Edition - Donald E. Knuth" for details.
*
*/

ast_t *newMixWValue             // the newly allocated AST node
(
    astNode_t   type,           // node type
    ast_t       *exp,           // minimal requirement for a W Value
    ast_t       *fPart,         // optional field specifier for the expression, may be NULL
    ast_t       *wValue         // optional existing W value, may be NULL
)
{
    ast_t *a = (malloc(sizeof(ast_t)));

    if (a != NULL)
    {
        a->type = type;
        a->sub.wValue.exp = exp;
        a->sub.wValue.fPart = fPart;
        a->sub.wValue.wValue = wValue;
    }
    return a;
}

/***************************************************************************************************
*
* newMixLiteral - allocate an AST node for a MIXAL literal
*
* A literal ia a W value. It will ultimately be turned into a CON directive inserted before the
* END directive of a MIX program.
* See "The Art of Computer Programming, 2nd Edition - Donald E. Knuth" for details.
*
*/

ast_t *newMixLiteral            // the newly allocated AST node
(
    astNode_t   type,           // node type
    ast_t       *wValue         // W value to be turned into a CON directive
)
{
    ast_t *a = (malloc(sizeof(ast_t)));

    if (a != NULL)
    {
        a->type = type;
        a->sub.literal.wValue = wValue;
    }
    return a;
}

/***************************************************************************************************
*
* newMixOp - allocate an AST node for a MIXAL operation (not a directive)
*
* An opcode may have either an A part with optional I and F parts or a literal.
*
*/

ast_t *newMixOp             // the newly allocated AST node
(
    astNode_t   type,       // node type
    uint8_t     c,          // the operations op code
    uint8_t     dF,         // a default F if no F part is defined
    ast_t       *aPart,     // address part, may be NULL
    ast_t       *iPart,     // optional I register index part, may be null
    ast_t       *fPart,     // optional field specifier, may be null
    ast_t       *wValue     // optional if no A [I] [F] parts are specified, may be NULL
)
{
    ast_t   *a = (malloc(sizeof(ast_t)));
    char    *newSym;

    if (a != NULL)
    {
        a->type = type;
        a->sub.op.c = c;
        a->sub.op.dF = dF;              // gets replaced if an F part is provided

        if (wValue == NULL)             // at least an A part
        {
            a->sub.op.aPart = aPart;
            a->sub.op.iPart = iPart;
            a->sub.op.fPart = fPart;
        }
        else                            // a literal
        {
            a->sub.op.aPart = generateAPart(&newSym);
            a->sub.op.iPart = NULL;
            a->sub.op.fPart = NULL;
            createNewCon(newSym, wValue);       // a literal always generates a new CON directive
        }
    }
    return a;
}

/***************************************************************************************************
*
* newMixDir - allocate an AST node for a MIXAL directive
*
* An directive may have either a W value or a string representing a symbol.
*
*/

ast_t *newMixDir                // the newly allocated AST node
(
    uint32_t    pseudoCode,     // the directive's pseudo op code, this will determine the node type
    ast_t       *wValue,        // optionsl W value, may be NULL
    char        *mixString      // symbol reperesenting the address of a W value, may be null
)
{
    ast_t *a = (malloc(sizeof(ast_t)));

    if (a != NULL)
    {
        assert (((pseudoCode == ALF) || (pseudoCode == CON) || (pseudoCode == END) ||
                (pseudoCode == EQU) || (pseudoCode == ORIG)));

        switch (pseudoCode)
        {
        case ALF:
            a->type = ALF_DIR_NODE;
            break;
        case CON:
            a->type = CON_DIR_NODE;
            break;
        case END:
            a->type = END_DIR_NODE;
            break;
        case EQU:
            a->type = EQU_DIR_NODE;
            break;
        case ORIG:
            a->type = ORIG_DIR_NODE;
            break;
        default:
            break;
        }
        a->sub.dir.wValue = wValue;
        a->sub.dir.mixString = mixString;
    }
    return a;
}

/***************************************************************************************************
*
* newMixDir - allocate an AST node for a MIXAL line
*
* A line is either a MIX operation or a MIX directive or a comment in the source code. Regardless
* of the type of line, its source code line is captured so that it can be added to the output
* file.
*
*/

ast_t *newMixLine           // the newly allocated AST node
(
    astNode_t   type,       // node type
    char        *label,     // optional label which gets recorded in the symbol table
    ast_t       *op,        // MIX operation AST, may be NULL
    ast_t       *dir        // MIX directive AST, may be NULL
)
{
    ast_t   *a = (malloc(sizeof(ast_t)));
    int32_t wValue;

    if (a != NULL)
    {
        char enhancedSrcLine[TMP_CHAR_BUF_SZ];

        a->type = type;

        assert((type == COMMENT_LINE_NODE) || (type == OP_LINE_NODE) || (type == DIR_LINE_NODE));

        if (type == COMMENT_LINE_NODE)
        {
            sprintf(enhancedSrcLine, "                %s", srcLine);    // capture src
            a->sub.line.srcLine = strdup(enhancedSrcLine);
            srcLinePtr = srcLine;
        }
        else if (type == OP_LINE_NODE)
        {
            sprintf(enhancedSrcLine, "# %04d(%04o):   %s", star, star, srcLine); // capture src
            a->sub.line.srcLine = strdup(enhancedSrcLine);
            srcLinePtr = srcLine;
            if (label != NULL) recordSymbol(label, op);     // record any preceeding label (symbol)
            a->sub.line.star = star++;                      // associate the current location (*)
        }
        else
        {
            switch (dir->type)  // directive line ASTs set up analoguously to operations
            {
                case ALF_DIR_NODE:
                case CON_DIR_NODE:
                    sprintf(enhancedSrcLine, "# %04d(%04o):   %s", star, star,  srcLine);
                    if (label != NULL) recordSymbol(label, dir);
                    a->sub.line.star = star++;      // W value generated, current location advanced
                    break;
                case ORIG_DIR_NODE:
                    sprintf(enhancedSrcLine, "#               %s", srcLine);
                    if (label != NULL) recordSymbol(label, dir);
                    star = evalAst(dir->sub.dir.wValue);        // ORIG alters the current location
                    a->sub.line.star = star;
                    break;
                case EQU_DIR_NODE:
                    sprintf(enhancedSrcLine, "#               %s", srcLine);
                    if (label != NULL) recordSymbol(label, dir);
                    a->sub.line.star = star;                    // no W value generated
                    break;
                case END_DIR_NODE:
                    if (label != NULL) recordSymbol(label, dir);
                    sprintf(enhancedSrcLine, "#               %s", srcLine);
                    a->sub.line.star = star;                    // no W value generated
                    break;
                default:
                    break;
            }
            a->sub.line.srcLine = strdup(enhancedSrcLine);  // source line formatted for output file
            srcLinePtr = srcLine;                           // reset source line buffer for new line
        }
        a->sub.line.op = op;
        a->sub.line.dir = dir;
    }
    return a;
}

/***************************************************************************************************
*
* freeAst - free a previously allocated AST node
*
* This function is recursively applied to free a whole AST.
*
*/

void freeAst
(
    ast_t *node     // AST node to be freed
)
{
    if (node != NULL)
    {
        switch (node->type)
        {
        case SYMBOL_NODE:
            free(node->sub.atomic.symbol);          // fall through
        case STAR_NODE:                             // fall through
        case NUMBER_NODE:                           // fall through
            break;
        case BINARY_EXP_NODE:
            freeAst(node->sub.exp.left);            // fall through
        case UNARY_EXP_NODE:
            freeAst(node->sub.exp.right);
            break;
        case A_NODE:                                // fall through
        case I_NODE:                                // fall through
        case F_NODE:
            freeAst(node->sub.aif.exp);
            break;
        case OP_NODE:
            freeAst(node->sub.op.aPart);
            freeAst(node->sub.op.iPart);
            freeAst(node->sub.op.fPart);
            break;
        case W_NODE:
            freeAst(node->sub.wValue.exp);
            freeAst(node->sub.wValue.fPart);
            freeAst(node->sub.wValue.wValue);
            break;
        case LITERAL_NODE:
            freeAst(node->sub.literal.wValue);
            break;
        case ALF_DIR_NODE:                          // fall through
        case CON_DIR_NODE:                          // fall through
        case END_DIR_NODE:                          // fall through
        case EQU_DIR_NODE:                          // fall through
        case ORIG_DIR_NODE:
            freeAst(node->sub.dir.wValue);
            free(node->sub.dir.mixString);
            break;
        case OP_LINE_NODE:                          // fall through
        case DIR_LINE_NODE:                         // fall through
        case COMMENT_LINE_NODE:
            free(node->sub.line.srcLine);
            freeAst(node->sub.line.op);
            freeAst(node->sub.line.dir);
            break;
        default:
            break;
        }
        free(node);
    }
}

/***************************************************************************************************
*
* asciiToAlf - transform an ascii string into a MIX alpha string
*
* The specification of an ALF string is 5 characters encoded according to the MIX character
* code mapping.
*
*/

static int32_t asciiToAlf       // an unsigned word with 5 MIX characters codes
(
    char *string                // ascii string to convert
)
{
    int32_t word = 0;
    char    buf[TMP_CHAR_BUF_SZ];

    sprintf(buf, "%s%s", string, "     ");              // defensive, pad with spaces
    for (uint32_t i = 0; i < CHARS_PER_MIX_STR; ++i)
    {
        char c = buf[i];

        word <<= BITS_PER_BYTE;                         // don't assume 8 bits per byte
        word |= (asciiMixMap[c] & BYTE_MASK);
    }
    return word;
}

/***************************************************************************************************
*
* evalAst - evaluate an AST node
*
* This function is recursively applied to evaluate a whole AST. If all symbols are resolved each
* AST evaluates to a signed integer i.e. a W value.
*
*/

int32_t evalAst
(
    ast_t *node
)
{
    symTabEntry_t   *symTabEntry;

    int32_t     tmpl;
    int32_t     tmpr;
    uint32_t    tmpf;
    uint32_t    tmpi;
    int32_t     tmpa;
    int32_t     w;
    int32_t     e;
    int32_t     f;
    bool        negate = false;
    int32_t     value = 0;

    if (node != NULL)
    {
        switch (node->type)
        {
        case SYMBOL_NODE:                                                               // terminal
            symTabEntry = lookUpSymbol(node->sub.atomic.symbol, node->sub.atomic.star);
            if (symTabEntry == NULL)
            {
                char buf[128];
                sprintf(buf,  "ERROR: Unknown symbol %s\n", node->sub.atomic.symbol);
                fprintf(stderr, "%s", buf); // let the user know
                fprintf(stdout, "%s", buf); // spike the assembled output file

                // let value default to the initialized value
            }
            else
            {
                value = symTabEntry->value;
            }
            break;
        case STAR_NODE:                                                                 // terminal
            value = node->sub.atomic.star;
            break;
        case NUMBER_NODE:                                                               // terminal
            value = node->sub.atomic.number;
            break;
        case UNARY_EXP_NODE:
            tmpr = evalAst(node->sub.exp.right);
            value = (node->sub.exp.expOp == '-') ? -tmpr : tmpr;
            break;
        case BINARY_EXP_NODE:
            tmpl = evalAst(node->sub.exp.left);
            tmpr = evalAst(node->sub.exp.right);
            switch (node->sub.exp.expOp)
            {
            case '+':
                value = tmpl + tmpr;
                break;
            case '-':
                value = tmpl - tmpr;
                break;
            case '*':
                value = tmpl * tmpr;
                break;
            case '/':
                value = tmpl / tmpr;
                break;
            case '%':
                value = tmpl % tmpr;
                break;
            case ':':
                value = (tmpl * LEFT_FIELD_MULTIPLIER) + tmpr;  // used for field definitions
                break;
            default:
                value = tmpr;
                break;
            }
            break;
        case A_NODE:                                // fall through
        case I_NODE:                                // fall through
        case F_NODE:
            value = evalAst(node->sub.aif.exp);
            break;
        case W_NODE:
            w = (node->sub.wValue.wValue == NULL) ? 0 : evalAst(node->sub.wValue.wValue);
            e = evalAst(node->sub.wValue.exp);
            f = (node->sub.wValue.fPart == NULL) ? DEFAULT_F : evalAst(node->sub.wValue.fPart);
            value = makeWValue(w, e, f);
            break;
        case LITERAL_NODE:
            value = evalAst(node->sub.literal.wValue);
            break;
        case OP_NODE:
            tmpa = (node->sub.op.aPart == NULL) ? 0 : evalAst(node->sub.op.aPart);
            if (tmpa < 0)
            {
                negate = true;
                tmpa = -tmpa;
            }
            tmpi = (node->sub.op.iPart == NULL) ? 0 : evalAst(node->sub.op.iPart);
            tmpf = (node->sub.op.fPart == NULL) ? node->sub.op.dF : evalAst(node->sub.op.fPart);
            value = (node->sub.op.c & 077) |
                    ((tmpf & 077) << 6) |
                    ((tmpi & 077) << 12) |
                    ((tmpa & 07777) << 18);
            value = (negate == true) ? -value : value;
            break;
        case ALF_DIR_NODE:
            value = asciiToAlf(node->sub.dir.mixString);
            break;
        case CON_DIR_NODE:                          // fall through
        case END_DIR_NODE:                          // fall through
        case EQU_DIR_NODE:                          // fall through
        case ORIG_DIR_NODE:
            value = evalAst(node->sub.dir.wValue);
            break;
        case OP_LINE_NODE:
            value = evalAst(node->sub.line.op);
            break;
        case DIR_LINE_NODE:
            value = evalAst(node->sub.line.dir);
            break;
        case COMMENT_LINE_NODE:
            break;
        default:
            value = 0;
            break;
        }
    }
    return value;
}

/***************************************************************************************************
*
* formatCommand - format a W value into a MIX simulator command
*
* The format is specific to a matched mixSym simulator. The commands are single characters such as
* W to load a word to the current memory pointer; M to set the current memory pointer; P to set
* the simulated program counter to a memory location, etc. See the help for mixSym. The numeric
* portion of the command is always a sign and magnitude. MIX words are defined as sign/magnitude;
* not 2s compliment. The numeric values are formatted as octal values because the mixSym byte is
* 6 bits.
*
*/

void formatCommand
(
    char cmd,       // typically one of 'W', 'P', 'M'
    int32_t word,   // numeric representation of the W value
    char *dst       // command is output to this string
)
{
    char sign = '+';
    uint32_t uword = 0;
    
    if (dst != NULL)
    {
        if (word < 0)
        {
            uword = -word;
            sign = '-';
        }
        else
        {
            uword = word;
        }
        sprintf(dst, "%c %c 0%010o", cmd, sign, uword);
    }
}

/***************************************************************************************************
*
* isLocal - determine if symbol is used for short jumps
*
* Check to see if symbol is of the form [0-9][HhFfBb]. This is the well established notation for
* creating local labels for short jumps; commonly used in assembly languages.
* See "The Art of Computer Programming, 2nd Edition - Donald E. Knuth" for details.
*
*/

static bool isLocal     // true if a local symbol, false otherwise
(
    char *symbol        // symbol to check
)
{
    bool result = false;
    
    bool hbf = ((symbol[1] == 'h') || (symbol[1] == 'H') ||
                (symbol[1] == 'b') || (symbol[1] == 'B') ||
                (symbol[1] == 'f') || (symbol[1] == 'F'));
    
    if ((strlen(symbol) == LOCAL_LABEL_SZ) &&
        ((symbol[0] >= '0') && (symbol[0] <= '9')) &&
        (hbf == true))
    {
        result = true;
    }
    
    return result;
}

/***************************************************************************************************
*
* lookUpSymbol - find the numeric value of a symbol
*
* Check the symbol table built up during the parsing phase of the assembler. The symbol can be
* a globally scoped symbol or a local label.
* See "The Art of Computer Programming, 2nd Edition - Donald E. Knuth" for details on symbols
* used as local labels for short jumps.
*
*/

static symTabEntry_t *lookUpSymbol      // the symbol table entry which has s matching symbol
(
    char *symbol,                       // symbol to be matched
    uint32_t star                       // current location; used for matching nearest local label
)
{
    symTabEntry_t *winner = NULL;
    
    if (isLocal(symbol))
    {
        symTabEntry_t *current = NULL;
        uint32_t start = 0;
        bool lookForward;

        lookForward = ((symbol[1] == 'F') || (symbol[1] == 'f')) ? true : false;

        do      // find the nearest matching local label
        {
            char partial[] = {symbol[0], 'H', '.', '\0'};
            current = lookUpNextPartial(partial, &start, &table);
            if ((current != NULL) && (start != 0))
            {
                if (current->value == star)
                {
                    winner = current;     // can't get any closer; done
                    break;
                }
                else if (lookForward == true)   // search forward for nearest matching label
                {
                    if (winner == NULL)
                    {
                        if (current->value > star) winner = current;
                    }
                    else if ((current->value < winner->value) &&
                             (current->value > star))
                    {
                        winner = current;
                    }
                }
                else                            // search backward for nearest matching label
                {
                    if (winner == NULL)
                    {
                        if (current->value < star) winner = current;
                    }
                    else if ((current->value > winner->value) &&
                             (current->value < star))
                    {
                        winner = current;
                    }
                }
            }
        } while ((current != NULL) && (start != 0));
    }
    else
    {
        winner = lookUp(symbol, &table);    // just a regular globally scoped symbol
    }

    return winner;
}

/***************************************************************************************************
*
* recordSymbol - record a symbol in the symbol table
*
* The symbol table allows for defining constants and future references (addresses.)
*
*/

void recordSymbol
(
    char *symbol,       // symbol to be added to symbol table
    ast_t *node         // node to be evaluated to get the symbol's value
)
{
    if (isLocal(symbol))
    {
        char tmp[TMP_CHAR_BUF_SZ];

        sprintf(tmp, "%c%c.%d", symbol[0], 'H', star);  // generate the symbol for a local label
        addEntry(strdup(tmp), star, &table);
        free(symbol);
    }
    else
    {
        if (node->type == EQU_DIR_NODE)                 // this directive is like a C #define
        {
            addEntry(symbol, evalAst(node), &table);    // symbol is set to the supplied value
        }
        else                                // symbol value is an address
        {
            addEntry(symbol, star, &table); // symbol is set to the value of the current location
        }
    }
}

/***************************************************************************************************
*
* splitF - splif an F part value into its left and right values
*
* An single F value is created according to the formula F = 8*left + right where left and right
* are the field specifier in an F part e.g. (0:5)
*
*/

static status_t splitF      // OK or ERROR
(
    uint32_t    f,          // f to be split
    uint32_t    *l,         // output: left value of the field
    uint32_t    *r
)
{
    status_t    result = OK;

    *l = (f & L_MASK) >> BITS_PER_NIBBLE;       // don't assume nibbles are 4 bits
    *r = (f & R_MASK);

    if ( (*l > MAX_L_R) ||
         (*r > MAX_L_R) ||
         (*r < *l) )
    {
    result = ERROR;
    }

    return result;
}

/***************************************************************************************************
*
* makeMask - create a mask for isolating the specified field in a MIX word
*
* The mask is right justified in the return value. The number of bits required to shift left is
* also returned to the caller.
* See "The Art of Computer Programming, 2nd Edition - Donald E. Knuth" for details on field
* specifiers (I parts aka index parts.)
*
*/

static uint32_t makeMask        // mask for isolating the field
(
    uint32_t    l,              // left part of the field specifier
    uint32_t    r,              // right part of the field specifier
    uint32_t    *shiftBits      // output: number of left shift bits required to position the mask
)
{
    uint32_t mask = 0;
    uint32_t widthBytes = 1;

    widthBytes += (r - l);
    *shiftBits = (MAX_L_R - r) * BITS_PER_BYTE;

    for (int w = 0; w < widthBytes; ++w)
    {
        mask = (mask << BITS_PER_BYTE) | BYTE_MASK;
    }

    return (mask << *shiftBits);
}

/***************************************************************************************************
*
* makeWValue - used to recursively assemble a W value
*
* W values are formed recursively.The expression part of a W value may be modified by a field
* specifier.
* See "The Art of Computer Programming, 2nd Edition - Donald E. Knuth" for details on field
* specifiers (I parts aka index parts.)
*
*/

int32_t makeWValue      // a 2s compliment representation of the W value
(
    int32_t w,          // W value used to recursively build a new W value
    int32_t e,          // value of the expression used to build the W value
    uint32_t f          // field specifier
)
{
    bool        moveSign = false;
    bool        wIsNegative = false;
    bool        eIsNegative = false;
    uint32_t    absW;
    uint32_t    absE;
    uint32_t    l;
    uint32_t    r;
    uint32_t    shift;
    uint32_t    mask;
    status_t    status;
    int32_t     result = 0;

    // the W value is built up as an absolute value and the sign is determined after

    if (w < 0)
    {
        wIsNegative = true;
        absW = -w;
    }
    else absW = w;

    if (e < 0)
    {
        eIsNegative = true;
        absE = -e;
    }
    else absE = e;

    status = splitF(f, &l, &r);
    if (status == OK)
    {
        if (l == 0)
        {
            moveSign = true;        // worry about the sign later
            l = 1;                  // just work on the magnitude for now
        }

        if (l <= r)
        {
            // take into account the field specifier

            mask = makeMask(l, r, &shift);
            absW &= ~mask;
            absE &= (mask >> shift);
            result = absW | (absE << shift);    // new W is built from the supplied W and expression

            // now determine the sign

            if (moveSign == true)
            {
                result = (eIsNegative) ? -result : result;
            }
            else
            {
                result = (wIsNegative) ? -result : result;
            }
        }
    }
    return result;
}

/***************************************************************************************************
*
* emitCommand - transform a W value to a MIX simulator command
*
* The format is specific to a matched mixSym simulator. The commands are single characters such as
* W to load a word to the current memory pointer; M to set the current memory pointer; P to set
* the simulated program counter to a memory location, etc. See the help for mixSym. The numeric
* portion of the command is always a sign and magnitude. MIX words are defined as sign/magnitude;
* not 2s compliment. The numeric values are formatted as octal values because the mixSym byte is
* 6 bits.
*
*/

void emitCommand
(
    ast_t *line     // AST holding a complete, parsed MIX assemble language line
)
{
    char    dst[TMP_CHAR_BUF_SZ];

    printf("%s\n", line->sub.line.srcLine);
    switch (line->type)
    {
        case OP_LINE_NODE:
            formatCommand('W', evalAst(line->sub.line.op), dst);
            printf("%s\n", dst);
            break;
        case DIR_LINE_NODE:
            switch (line->sub.line.dir->type)
            {
                case ALF_DIR_NODE:                  // fall through
                case CON_DIR_NODE:
                    formatCommand('W', evalAst(line->sub.line.dir), dst);
                    printf("%s\n", dst);
                    break;
                case END_DIR_NODE:
                    formatCommand('P', evalAst(line->sub.line.dir), dst);
                    printf("%s\n", dst);
                    break;
                case ORIG_DIR_NODE:
                    formatCommand('M', evalAst(line->sub.line.dir), dst);
                    printf("%s\n", dst);
                    break;
                case EQU_DIR_NODE:                  // no command emitted
                default:
                    break;
            }
            break;
        case COMMENT_LINE_NODE:                     // no command emitted
        default:
            break;
    }
}

/***************************************************************************************************
*
* genSymbolName - genarate a new symbol name for a literal
*
*
*/

void genSymbolName
(
    char *dst       // buffer in which to return the newly generated symbol
)
{
    static uint32_t n = 0;

    sprintf(dst, "=CON%d", n);  // use con in the name: literals are transformed into CON directives
    n += 1;
}

/***************************************************************************************************
*
* generateAPart - build up a new A part from a symbol
*
* The symbol is used to refer to a new CON generated from a literal. A new symbol is generated
* and returned to the caller along with the A part built up from the new symbol.
*
*/

ast_t *generateAPart    // new A part used to replace an operation's literal
(
    char **newSym       // output: pointer to the newly generated symbol used to build up the A part
)
{
    char    dst[TMP_CHAR_BUF_SZ];
    ast_t   *aPart;
    ast_t   *exp;
    ast_t   *atomic;

    genSymbolName(dst);         // generate new symbol
    *newSym = strdup(dst);      // place it where the caller can see it

    // build up the new A part

    atomic = newMixAtomic(SYMBOL_NODE, 0, *newSym);
    exp = newMixExp(UNARY_EXP_NODE, NULL, '+', atomic);
    aPart = newMixAif(A_NODE, exp);

    return aPart;
}

/***************************************************************************************************
*
* createNewCon - generate a new CON directive to store the W value of a literal
*
* Literals cause a new CON directive to store a W value. The new CON is inserted just before
* the END directive.
*
*/

void createNewCon
(
    char *newSym,   // symbol used to reference the W value
    ast_t *wValue   // AST node with the w Value
)
{
    char generatedSrcLine[TMP_CHAR_BUF_SZ];

    ast_t *dir = newMixDir(CON, wValue, NULL);
    ast_t *line = newMixLine(DIR_LINE_NODE, newSym, NULL, dir);
    
    star--; // the newly created con directive is inserted before the end

    sprintf(generatedSrcLine, "# <literal>     %s\tCON <w_value>", newSym);
    line->sub.line.srcLine = strdup(generatedSrcLine);

    enqueue(line, &literalListTail);    // add it to a list of internally generated CONs
}

/***************************************************************************************************
*
* spliceAndFixup - add all the new CON directives generated by literals before the END directive
*
* A separate list of CON directives is built along the way as the source file is parsed. Once the
* list of ASTs is produced, the extra CONs are spliced into the linked list of line ASTs. The
* addresses for the new CONs and END directives are then fixed up.
*
*/

void spliceAndFixup(void)
{
    listEntry_t *nextToLastLine = lineListTail->previous;
    listEntry_t *lastLine = lineListTail;
    listEntry_t *tmp = literalList.next;
    uint32_t    numLiterals = 0;
    uint32_t    endLoc = lastLine->ast->sub.line.star;

    // run the literal list to find out how many literals (CONs) were added

    while (tmp != NULL)
    {
        numLiterals++;
        tmp = tmp->next;
    }

    if (numLiterals > 0)
    {
        symTabEntry_t   *endCardSymTabEntry = NULL;
        symTabEntry_t   *sp = NULL;

        // check to see if the end card has an entry in the symbol table

        endCardSymTabEntry = lookUpByValue(endLoc, &table);

        // splice the literal list in just before the last line entry, i.e. the END directive

        nextToLastLine->next = literalList.next;
        literalList.next->previous = nextToLastLine;
        literalListTail->next = lastLine;
        lastLine->previous = literalListTail;

        // fix up the symbol table values for the literals

        uint32_t suffix = 0;
        uint32_t star = endLoc;
        while (true)
        {
            char            conName[TMP_CHAR_BUF_SZ];

            sprintf(conName, "=CON%d", suffix++);
            sp = lookUp(conName, &table);
            if (sp == NULL)
            {
                break;
            }
            else
            {
                sp->value = star++;     // add the correct value for the symbol
            }
        }

        // fix up the END line's' symbol value if there is one

        if (endCardSymTabEntry != NULL) endCardSymTabEntry->value = star;
    }
}

/***************************************************************************************************
*
* freeAllAstNodes - return all allocated memory
*
* This includes all the memory allocated for the AST nodes, string dups and lined list nodes.
* NOTE: the string dups are freed by freeAst().
*
*/

void freeAllAstNodes(void)
{
    listEntry_t *tmp = lineList.next;    // skip the dummy head
    
    /*
     * Run the line list to free all the AST nodes.
     * The literals have been spliced into the line list at this point.
     */

    while (tmp != NULL)
    {
        freeAst(tmp->ast);
        tmp = tmp->next;
    }

    // run the list backwards and free the list nodes

    tmp = lineListTail;
    while (tmp->previous != NULL)
    {
        tmp = tmp->previous;
        free(tmp->next);
    }
}

