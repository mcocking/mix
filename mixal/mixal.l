/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
05mar23 m_c  improved white space matching
04mar23 m_c  extend character codes to include '%' and '#'
04mar23 m_c  allow '#' as comment character
04mar23 m_c  use single quotes to enclose ALF strings
04mar23 m_c  support inline comments
04mar23 m_c  change // operator symbol to % to implement modulo
26feb23 m_c  added %option noyywrap
14jul20 m_c  initial version
*/


/* 
 * Description: MIXAL scanner
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */

%option noyywrap
%option yylineno

%{

#include "assembler.h"
#include "mixal.tab.h"

static void appendToSrcLine(char *text);
static int finish(int token);

%}

digit       [0-9]
letter      [a-zA-Z]

%%

"," |
"(" |
")" |
"=" |
"+" |
"-" |
"*" |
"/" |
"%" |
":"     {return (finish(yytext[0]));}

    /* MIX instructions */

[Nn][Oo][Pp]            {yylval.code.c = 0; yylval.code.f = 0; return (finish(CODE));}
[Aa][Dd][Dd]            {yylval.code.c = 1; yylval.code.f = 5; return (finish(CODE));}
[Ff][Aa][Dd][Dd]        {yylval.code.c = 1; yylval.code.f = 6; return (finish(CODE));}
[Ss][Uu][Bb]            {yylval.code.c = 2; yylval.code.f = 5; return (finish(CODE));}
[Ff][Ss][Uu][Bb]        {yylval.code.c = 2; yylval.code.f = 6; return (finish(CODE));}
[Mm][Uu][Ll]            {yylval.code.c = 3; yylval.code.f = 5; return (finish(CODE));}
[Ff][Mm][Uu][Ll]        {yylval.code.c = 3; yylval.code.f = 6; return (finish(CODE));}
[Dd][Ii][Vv]            {yylval.code.c = 4; yylval.code.f = 5; return (finish(CODE));}
[Ff][Dd][Ii][Vv]        {yylval.code.c = 4; yylval.code.f = 6; return (finish(CODE));}
[Nn][Uu][Mm]            {yylval.code.c = 5; yylval.code.f = 0; return (finish(CODE));}
[Cc][Hh][Aa][Rr]        {yylval.code.c = 5; yylval.code.f = 1; return (finish(CODE));}
[Hh][Ll][Tt]            {yylval.code.c = 5; yylval.code.f = 2; return (finish(CODE));}
[Ss][Ll][Aa]            {yylval.code.c = 6; yylval.code.f = 0; return (finish(CODE));}
[Ss][Rr][Aa]            {yylval.code.c = 6; yylval.code.f = 1; return (finish(CODE));}
[Ss][Ll][Aa][Xx]        {yylval.code.c = 6; yylval.code.f = 2; return (finish(CODE));}
[Ss][Rr][Aa][Xx]        {yylval.code.c = 6; yylval.code.f = 3; return (finish(CODE));}
[Ss][Ll][Cc]            {yylval.code.c = 6; yylval.code.f = 4; return (finish(CODE));}
[Ss][Rr][Cc]            {yylval.code.c = 6; yylval.code.f = 5; return (finish(CODE));}
[Mm][Oo][Vv][Ee]        {yylval.code.c = 7; yylval.code.f = 1; return (finish(CODE));}
[Ll][Dd][Aa]            {yylval.code.c = 8; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][1]             {yylval.code.c = 9; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][2]             {yylval.code.c = 10; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][3]             {yylval.code.c = 11; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][4]             {yylval.code.c = 12; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][5]             {yylval.code.c = 13; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][6]             {yylval.code.c = 14; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][Xx]            {yylval.code.c = 15; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][Aa][Nn]        {yylval.code.c = 16; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][1][Nn]         {yylval.code.c = 17; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][2][Nn]         {yylval.code.c = 18; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][3][Nn]         {yylval.code.c = 19; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][4][Nn]         {yylval.code.c = 20; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][5][Nn]         {yylval.code.c = 21; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][6][Nn]         {yylval.code.c = 22; yylval.code.f = 5; return (finish(CODE));}
[Ll][Dd][Xx][Nn]        {yylval.code.c = 23; yylval.code.f = 5; return (finish(CODE));}
[Ss][Tt][Aa]            {yylval.code.c = 24; yylval.code.f = 5; return (finish(CODE));}
[Ss][Tt][1]             {yylval.code.c = 25; yylval.code.f = 5; return (finish(CODE));}
[Ss][Tt][2]             {yylval.code.c = 26; yylval.code.f = 5; return (finish(CODE));}
[Ss][Tt][3]             {yylval.code.c = 27; yylval.code.f = 5; return (finish(CODE));}
[Ss][Tt][4]             {yylval.code.c = 28; yylval.code.f = 5; return (finish(CODE));}
[Ss][Tt][5]             {yylval.code.c = 29; yylval.code.f = 5; return (finish(CODE));}
[Ss][Tt][6]             {yylval.code.c = 30; yylval.code.f = 5; return (finish(CODE));}
[Ss][Tt][Xx]            {yylval.code.c = 31; yylval.code.f = 5; return (finish(CODE));}
[Ss][Tt][Jj]            {yylval.code.c = 32; yylval.code.f = 2; return (finish(CODE));}
[Ss][Tt][Zz]            {yylval.code.c = 33; yylval.code.f = 5; return (finish(CODE));}
[Jj][Bb][Uu][Ss]        {yylval.code.c = 34; yylval.code.f = 0; return (finish(CODE));}
[Ii][Oo][Cc]            {yylval.code.c = 35; yylval.code.f = 0; return (finish(CODE));}
[Ii][Nn]                {yylval.code.c = 36; yylval.code.f = 0; return (finish(CODE));}
[Oo][Uu][Tt]            {yylval.code.c = 37; yylval.code.f = 0; return (finish(CODE));}
[Jj][Rr][Ee][Dd]        {yylval.code.c = 38; yylval.code.f = 0; return (finish(CODE));}
[Jj][Mm][Pp]            {yylval.code.c = 39; yylval.code.f = 0; return (finish(CODE));}
[Jj][Ss][Jj]            {yylval.code.c = 39; yylval.code.f = 1; return (finish(CODE));}
[Jj][Oo][Vv]            {yylval.code.c = 39; yylval.code.f = 2; return (finish(CODE));}
[JJ][Nn][Oo][Vv]        {yylval.code.c = 39; yylval.code.f = 3; return (finish(CODE));}
[Jj][Ll]                {yylval.code.c = 39; yylval.code.f = 4; return (finish(CODE));}
[Jj][Ee]                {yylval.code.c = 39; yylval.code.f = 5; return (finish(CODE));}
[Jj][Gg]                {yylval.code.c = 39; yylval.code.f = 6; return (finish(CODE));}
[Jj][Gg][Ee]            {yylval.code.c = 39; yylval.code.f = 7; return (finish(CODE));}
[Jj][Nn][Ee]            {yylval.code.c = 39; yylval.code.f = 8; return (finish(CODE));}
[Jj][Ll][Ee]            {yylval.code.c = 39; yylval.code.f = 9; return (finish(CODE));}
[Jj][Aa][Nn]            {yylval.code.c = 40; yylval.code.f = 0; return (finish(CODE));}
[Jj][Aa][Zz]            {yylval.code.c = 40; yylval.code.f = 1; return (finish(CODE));}
[Jj][Aa][Pp]            {yylval.code.c = 40; yylval.code.f = 2; return (finish(CODE));}
[Jj][Aa][Nn][Nn]        {yylval.code.c = 40; yylval.code.f = 3; return (finish(CODE));}
[Jj][Aa][Nn][Zz]        {yylval.code.c = 40; yylval.code.f = 4; return (finish(CODE));}
[Jj][Aa][Nn][Pp]        {yylval.code.c = 40; yylval.code.f = 5; return (finish(CODE));}
[Jj][1][Nn]             {yylval.code.c = 41; yylval.code.f = 0; return (finish(CODE));}
[Jj][1][Zz]             {yylval.code.c = 41; yylval.code.f = 1; return (finish(CODE));}
[Jj][1][Pp]             {yylval.code.c = 41; yylval.code.f = 2; return (finish(CODE));}
[Jj][1]][Nn][Nn]        {yylval.code.c = 41; yylval.code.f = 3; return (finish(CODE));}
[Jj][1][Nn][Zz]         {yylval.code.c = 41; yylval.code.f = 4; return (finish(CODE));}
[Jj][1][Nn][Pp]         {yylval.code.c = 41; yylval.code.f = 5; return (finish(CODE));}
[Jj][2][Nn]             {yylval.code.c = 42; yylval.code.f = 0; return (finish(CODE));}
[Jj][2][Zz]             {yylval.code.c = 42; yylval.code.f = 1; return (finish(CODE));}
[Jj][2][Pp]             {yylval.code.c = 42; yylval.code.f = 2; return (finish(CODE));}
[Jj][2]][Nn][Nn]        {yylval.code.c = 42; yylval.code.f = 3; return (finish(CODE));}
[Jj][2][Nn][Zz]         {yylval.code.c = 42; yylval.code.f = 4; return (finish(CODE));}
[Jj][2][Nn][Pp]         {yylval.code.c = 42; yylval.code.f = 5; return (finish(CODE));}
[Jj][3][Nn]             {yylval.code.c = 43; yylval.code.f = 0; return (finish(CODE));}
[Jj][3][Zz]             {yylval.code.c = 43; yylval.code.f = 1; return (finish(CODE));}
[Jj][3][Pp]             {yylval.code.c = 43; yylval.code.f = 2; return (finish(CODE));}
[Jj][3]][Nn][Nn]        {yylval.code.c = 43; yylval.code.f = 3; return (finish(CODE));}
[Jj][3][Nn][Zz]         {yylval.code.c = 43; yylval.code.f = 4; return (finish(CODE));}
[Jj][3][Nn][Pp]         {yylval.code.c = 43; yylval.code.f = 5; return (finish(CODE));}
[Jj][4][Nn]             {yylval.code.c = 44; yylval.code.f = 0; return (finish(CODE));}
[Jj][4][Zz]             {yylval.code.c = 44; yylval.code.f = 1; return (finish(CODE));}
[Jj][4][Pp]             {yylval.code.c = 44; yylval.code.f = 2; return (finish(CODE));}
[Jj][4]][Nn][Nn]        {yylval.code.c = 44; yylval.code.f = 3; return (finish(CODE));}
[Jj][4][Nn][Zz]         {yylval.code.c = 44; yylval.code.f = 4; return (finish(CODE));}
[Jj][4][Nn][Pp]         {yylval.code.c = 44; yylval.code.f = 5; return (finish(CODE));}
[Jj][5][Nn]             {yylval.code.c = 45; yylval.code.f = 0; return (finish(CODE));}
[Jj][5][Zz]             {yylval.code.c = 45; yylval.code.f = 1; return (finish(CODE));}
[Jj][5][Pp]             {yylval.code.c = 45; yylval.code.f = 2; return (finish(CODE));}
[Jj][5]][Nn][Nn]        {yylval.code.c = 45; yylval.code.f = 3; return (finish(CODE));}
[Jj][5][Nn][Zz]         {yylval.code.c = 45; yylval.code.f = 4; return (finish(CODE));}
[Jj][5][Nn][Pp]         {yylval.code.c = 45; yylval.code.f = 5; return (finish(CODE));}
[Jj][6][Nn]             {yylval.code.c = 46; yylval.code.f = 0; return (finish(CODE));}
[Jj][6][Zz]             {yylval.code.c = 46; yylval.code.f = 1; return (finish(CODE));}
[Jj][6][Pp]             {yylval.code.c = 46; yylval.code.f = 2; return (finish(CODE));}
[Jj][6]][Nn][Nn]        {yylval.code.c = 46; yylval.code.f = 3; return (finish(CODE));}
[Jj][6][Nn][Zz]         {yylval.code.c = 46; yylval.code.f = 4; return (finish(CODE));}
[Jj][6][Nn][Pp]         {yylval.code.c = 46; yylval.code.f = 5; return (finish(CODE));}
[Jj][Xx][Nn]            {yylval.code.c = 47; yylval.code.f = 0; return (finish(CODE));}
[Jj][Xx][Zz]            {yylval.code.c = 47; yylval.code.f = 1; return (finish(CODE));}
[Jj][Xx][Pp]            {yylval.code.c = 47; yylval.code.f = 2; return (finish(CODE));}
[Jj][Xx][Nn][Nn]        {yylval.code.c = 47; yylval.code.f = 3; return (finish(CODE));}
[Jj][Xx][Nn][Zz]        {yylval.code.c = 47; yylval.code.f = 4; return (finish(CODE));}
[Jj][Xx][Nn][Pp]        {yylval.code.c = 47; yylval.code.f = 5; return (finish(CODE));}
[Ii][Nn][Cc][Aa]        {yylval.code.c = 48; yylval.code.f = 0; return (finish(CODE));}
[Dd][Ee][Cc][Aa]        {yylval.code.c = 48; yylval.code.f = 1; return (finish(CODE));}
[Ee][Nn][Tt][Aa]        {yylval.code.c = 48; yylval.code.f = 2; return (finish(CODE));}
[Ee][Nn][Nn][Aa]        {yylval.code.c = 48; yylval.code.f = 3; return (finish(CODE));}
[Ii][Nn][Cc][1]         {yylval.code.c = 49; yylval.code.f = 0; return (finish(CODE));}
[Dd][Ee][Cc][1]         {yylval.code.c = 49; yylval.code.f = 1; return (finish(CODE));}
[Ee][Nn][Tt][1]         {yylval.code.c = 49; yylval.code.f = 2; return (finish(CODE));}
[Ee][Nn][Nn][1]         {yylval.code.c = 49; yylval.code.f = 3; return (finish(CODE));}
[Ii][Nn][Cc][2]         {yylval.code.c = 50; yylval.code.f = 0; return (finish(CODE));}
[Dd][Ee][Cc][2]         {yylval.code.c = 50; yylval.code.f = 1; return (finish(CODE));}
[Ee][Nn][Tt][2]         {yylval.code.c = 50; yylval.code.f = 2; return (finish(CODE));}
[Ee][Nn][Nn][2]         {yylval.code.c = 50; yylval.code.f = 3; return (finish(CODE));}
[Ii][Nn][Cc][3]         {yylval.code.c = 51; yylval.code.f = 0; return (finish(CODE));}
[Dd][Ee][Cc][3]         {yylval.code.c = 51; yylval.code.f = 1; return (finish(CODE));}
[Ee][Nn][Tt][3]         {yylval.code.c = 51; yylval.code.f = 2; return (finish(CODE));}
[Ee][Nn][Nn][3]         {yylval.code.c = 51; yylval.code.f = 3; return (finish(CODE));}
[Ii][Nn][Cc][4]         {yylval.code.c = 52; yylval.code.f = 0; return (finish(CODE));}
[Dd][Ee][Cc][4]         {yylval.code.c = 52; yylval.code.f = 1; return (finish(CODE));}
[Ee][Nn][Tt][4]         {yylval.code.c = 52; yylval.code.f = 2; return (finish(CODE));}
[Ee][Nn][Nn][4]         {yylval.code.c = 52; yylval.code.f = 3; return (finish(CODE));}
[Ii][Nn][Cc][5]         {yylval.code.c = 53; yylval.code.f = 0; return (finish(CODE));}
[Dd][Ee][Cc][5]         {yylval.code.c = 53; yylval.code.f = 1; return (finish(CODE));}
[Ee][Nn][Tt][5]         {yylval.code.c = 53; yylval.code.f = 2; return (finish(CODE));}
[Ee][Nn][Nn][5]         {yylval.code.c = 53; yylval.code.f = 3; return (finish(CODE));}
[Ii][Nn][Cc][6]         {yylval.code.c = 54; yylval.code.f = 0; return (finish(CODE));}
[Dd][Ee][Cc][6]         {yylval.code.c = 54; yylval.code.f = 1; return (finish(CODE));}
[Ee][Nn][Tt][6]         {yylval.code.c = 54; yylval.code.f = 2; return (finish(CODE));}
[Ee][Nn][Nn][6]         {yylval.code.c = 54; yylval.code.f = 3; return (finish(CODE));}
[Ii][Nn][Cc][Xx]        {yylval.code.c = 55; yylval.code.f = 0; return (finish(CODE));}
[Dd][Ee][Cc][Xx]        {yylval.code.c = 55; yylval.code.f = 1; return (finish(CODE));}
[Ee][Nn][Tt][Xx]        {yylval.code.c = 55; yylval.code.f = 2; return (finish(CODE));}
[Ee][Nn][Nn][Xx]        {yylval.code.c = 55; yylval.code.f = 3; return (finish(CODE));}
[Cc][Mm][Pp][Aa]        {yylval.code.c = 56; yylval.code.f = 5; return (finish(CODE));}
[Ff][Cc][Mm][Pp][Aa]    {yylval.code.c = 56; yylval.code.f = 6; return (finish(CODE));}
[Cc][Mm][Pp][1]         {yylval.code.c = 57; yylval.code.f = 5; return (finish(CODE));}
[Cc][Mm][Pp][2]         {yylval.code.c = 58; yylval.code.f = 5; return (finish(CODE));}
[Cc][Mm][Pp][3]         {yylval.code.c = 59; yylval.code.f = 5; return (finish(CODE));}
[Cc][Mm][Pp][4]         {yylval.code.c = 60; yylval.code.f = 5; return (finish(CODE));}
[Cc][Mm][Pp][5]         {yylval.code.c = 61; yylval.code.f = 5; return (finish(CODE));}
[Cc][Mm][Pp][6]         {yylval.code.c = 62; yylval.code.f = 5; return (finish(CODE));}
[Cc][Mm][Pp][Xx]        {yylval.code.c = 63; yylval.code.f = 5; return (finish(CODE));}

    /* MIX directives (assigned pseudo-opcodes) */

[Aa][Ll][Ff]            {yylval.num = ALF; return (finish(DIRECTIVE));}
[Cc][Oo][Nn]            {yylval.num = CON; return (finish(DIRECTIVE));}
[Ee][Nn][Dd]            {yylval.num = END; return (finish(DIRECTIVE));}
[Ee][Qq][Uu]            {yylval.num = EQU; return (finish(DIRECTIVE));}
[Oo][Rr][Ii][Gg]        {yylval.num = ORIG; return (finish(DIRECTIVE));}

    /* number as defined by the MIX architecture spec */

{digit}{1,10} {yylval.num = strtoul(yytext, NULL, 10); return (finish(NUMBER));}

    /* symbol as defined by the MIX architecture spec */

({digit}|{letter}){1,10} {yylval.sym = strndup(yytext, 10); return (finish(SYMBOL));}

    /* alpha string as defined by the MIX architecture spec */

"'".{5}"'" {yylval.sym = strndup(&(yytext[1]), 5); return (finish(ALFSTRING));}

    /* full line comments need to generate a node */

^[ \t\v\f\r]*"#".*$ {return (finish(COMMENT));}   // extend mixal to allow '#'
^[ \t\v\f\r]*"*".*$ {return (finish(COMMENT));}   // traditional mixal comment

\n      {return EOL;}

"#".*$ {appendToSrcLine(yytext);}  // treat inline comments as whitespace

[ \t\v\f\r]+ {appendToSrcLine(yytext);}

    /* unmatched */

.       {yyerror("%s\n", yytext);}

%%

/***************************************************************************************************
*
* appendToSrcLine - accumulate the source code line
*
* The text of the source code line is accumulated so that it can be added to the assembled output
* to provide a mixed code output.
*
*/

static void appendToSrcLine
(
    char *text                  // the text to be accumulated into a src line
)
{
    sprintf(srcLinePtr, "%s", text);
    srcLinePtr += strlen(text);
}

/***************************************************************************************************
*
* finish - record the token string and return the token to the parser
*
*/

static int finish
(
    int token           // the token to be returned to the parser
)
{
    appendToSrcLine(yytext);
    return token;
}
