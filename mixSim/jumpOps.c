/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/


/*
 * Description: Implement the MIX jump operations. Some conditional versions of these operations
 * check the MIX L, E and G flags. Other conditional versions check local (static) flags after
 * an ad hoc test.
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */

#include <stdint.h>
#include <stdbool.h>

#include "mixDefs.h"
#include "mixTypes.h"
#include "mixDecode.h"
#include "mix.h"

static bool n = false;
static bool z = true;
static bool p = false;

/***************************************************************************************************
*
* testReg - test a regiser to determine if it is negative, zero or positive
*
* The local flags n, z and p are set/cleared depending on the value of the register.
*
*/

static void testReg
(
    mixData_t   *reg        // register to be tested
)
{

    if (reg->integer.magnitude == 0)        // -0 and +0 are considered 0
    {
        n = false; z = true; p = false;
    }
    else if (reg->integer.sign == 1)
    {
        n = true; z = false; p = false;
    }
    else
    {
        n = false; z = false; p = true;
    }
}

/***************************************************************************************************
*
* testLEG - check the machine's L, E and G flags to determine if a jump should be taken
*
*/

static status_t testLEG         // OK if successful or BAD_F otherwise
(
    uint32_t    f,              // F part indicating instruction variant
    bool        *takeJump       // output: true if jump should be taken, false otherwise
)
{
    status_t result = OK;

    switch (f)
    {

        case JMP_VARIANT:               // fall through
        case JSJ_VARIANT:
            *takeJump = true;           // unconditional jumps
            break;
        case JOV_VARIANT:
            *takeJump = (V == true);
            break;
        case JNOV_VARIANT:
            *takeJump = (V != true);
            break;
        case JL_VARIANT:
            *takeJump = (L == true);
            break;
        case JE_VARIANT:
            *takeJump = (E == true);
            break;
        case JG_VARIANT:
            *takeJump = (G == true);
            break;
        case JGE_VARIANT:
            *takeJump = (L != true);
            break;
        case JNE_VARIANT:
            *takeJump = (E != true);
            break;
        case JLE_VARIANT:
            *takeJump = (G != true);
            break;
        default:
            result = BAD_F;
            break;
    }

    return result;
}

/***************************************************************************************************
*
* testNZP - check the N, Z and P flags to determine if a jump should be taken
*
* The N, Z and P flags are local (static) flags set are set/cleared after a call to testReg()
*
*/

static status_t testNZP         // OK if successful or BAD_F otherwise
(
    uint32_t    f,              // F part indicating instruction variant
    bool        *takeJump       // output: true if jump should be taken, false otherwise
)
{
    status_t result = OK;

    switch (f)
    {
        case JN_VARIANT:
            *takeJump = (n == true);
            break;
        case JZ_VARIANT:
            *takeJump = (z == true);
            break;
        case JP_VARIANT:
            *takeJump = (p == true);
            break;
        case JNN_VARIANT:
            *takeJump = (n != true);
            break;
        case JNZ_VARIANT:
            *takeJump = (z != true);
            break;
        case JNP_VARIANT:
            *takeJump = (p != true);
            break;
        default:
            result = BAD_F;
            break;
    }

    return result;
}

/***************************************************************************************************
*
* jmp - implement the MIX jmp instruction
*
* This includes all instruction variants which test the L, E and G flags.
*
*/

status_t jmp            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    bool        takeJump = false;
    status_t    result = testLEG(f, &takeJump);

    if (result == OK)
    {
        if (takeJump == true)
        {
            if (f == JOV_VARIANT)
            {
                V = false;                          // reset the overflow toggle
            }

            if (f != JSJ_VARIANT)
            {
                J.integer.magnitude = pc;           // save return address in J (link) register
            }

            pc = mixIntTo2sC(&(I[i])) + addr;       // tale the jump
        }
        else
        {
            if (f == JNOV_VARIANT)
            {
                V = false;                          // reset the overflow toggle
            }
        }
    }

    return result;
}

/***************************************************************************************************
*
* regJump - test a register value and take the jump if the condition is met
*
*/

status_t regJump        // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    bool        takeJump = false;
    status_t    result = testNZP(f, &takeJump);

    if (result == OK)
    {
        if (takeJump == true)
        {
            J.integer.magnitude = pc;
            pc = mixIntTo2sC(&(I[i])) + addr;
        }
    }

    return result;
}

/***************************************************************************************************
*
* ja - implement the MIX ja operation
*
* Test the A register value and take the jump if the condition is met. The F part specifies the
* condition to test.
*
*/

status_t ja             // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode
    uint32_t    f,      // F part (specifies condition to test)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    testReg(&A);
    return (regJump(c, f, i, addr));
}

/***************************************************************************************************
*
* jx - implement the MIX jx operation
*
* Test the X register value and take the jump if the condition is met. The F part specifies the
* condition to test.
*
*/

status_t jx             // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode
    uint32_t    f,      // F part (specifies condition to test)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    testReg(&X);
    return (regJump(c, f, i, addr));
}

/***************************************************************************************************
*
* mixj1 - implement the MIX j1 operation
*
* Test the I1 register value and take the jump if the condition is met. The F part specifies the
* condition to test.
*
*/

status_t mixj1          // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode
    uint32_t    f,      // F part (specifies condition to test)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    testReg(&(I[1]));
    return (regJump(c, f, i, addr));
}

/***************************************************************************************************
*
* j2 - implement the MIX j2 operation
*
* Test the I2 register value and take the jump if the condition is met. The F part specifies the
* condition to test.
*
*/

status_t j2             // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode
    uint32_t    f,      // F part (specifies condition to test)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    testReg(&(I[2]));
    return (regJump(c, f, i, addr));
}

/***************************************************************************************************
*
* j3 - implement the MIX j3 operation
*
* Test the I3 register value and take the jump if the condition is met. The F part specifies the
* condition to test.
*
*/

status_t j3             // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode
    uint32_t    f,      // F part (specifies condition to test)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    testReg(&I[3]);
    return (regJump(c, f, i, addr));
}

/***************************************************************************************************
*
* j4 - implement the MIX j4 operation
*
* Test the I4 register value and take the jump if the condition is met. The F part specifies the
* condition to test.
*
*/

status_t j4             // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode
    uint32_t    f,      // F part (specifies condition to test)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    testReg(&(I[4]));
    return (regJump(c, f, i, addr));
}

/***************************************************************************************************
*
* j5 - implement the MIX j5 operation
*
* Test the I5 register value and take the jump if the condition is met. The F part specifies the
* condition to test.
*
*/

status_t j5             // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode
    uint32_t    f,      // F part (specifies condition to test)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    testReg(&(I[5]));
    return (regJump(c, f, i, addr));
}

/***************************************************************************************************
*
* j6 - implement the MIX j6 operation
*
* Test the I6 register value and take the jump if the condition is met. The F part specifies the
* condition to test.
*
*/

status_t j6             // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode
    uint32_t    f,      // F part (specifies condition to test)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    testReg(&(I[6]));
    return (regJump(c, f, i, addr));
}
