/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/


#ifndef MIX_DEFS_H
#define MIX_DEFS_H

#define TMP_BUF_SZ          256

#define NUM_OPCODES         64          // C field of instruction is 6 bits
#define NUM_MEMORY_CELLS    4096        // must be power of 2
#define MAX_MEMORY_CELL     (NUM_MEMORY_CELLS - 1)
#define NUM_I_REGS          7           // I[0] hardwired to 0
#define BITS_PER_BYTE       6
#define BITS_PER_NIBBLE     3
#define BYTES_PER_MAGNITUDE 5
#define BITS_PER_MAGNITUDE  (BITS_PER_BYTE * BYTES_PER_MAGNITUDE)
#define BYTE_MASK           077         // note octal notation
#define MIN_L_R             0
#define MAX_L_R             5
#define MAX_L_R_ADDR        2
#define R_MASK              07          // F = 8L + R (note octal notation)
#define L_MASK              (R_MASK << BITS_PER_NIBBLE)
#define MAGNITUDE_MASK      07777777777 // note octal notation

// IO units

#define NUM_IO_UNITS        20

#define MAG_TAPE_0          0
#define MAG_TAPE_1          1
#define MAG_TAPE_2          2
#define MAG_TAPE_3          3
#define MAG_TAPE_4          4
#define MAG_TAPE_5          5
#define MAG_TAPE_6          6
#define MAG_TAPE_7          7
#define DISK_0              8
#define DISK_1              9
#define DISK_2              10
#define DISK_3              11
#define DISK_4              12
#define DISK_5              13
#define DISK_6              14
#define DISK_7              15
#define CARD_READER         16
#define CARD_PUNCH          17
#define PRINTER             18
#define PAPER_TAPE          19

#define M_TAPE_BLOCK_SIZE   100         // words
#define DISK_BLOCK_SIZE     100         // words
#define CARD_BLOCK_SIZE     16          // words
#define PRINTER_BLOCK_SIZE  24          // words
#define P_TAPE_BLOCK_SIZE   14          // words

#define PRINTER_PAGE_HEIGHT 25
#define PRINTER_LINE_WIDTH  (PRINTER_BLOCK_SIZE * BYTES_PER_MAGNITUDE)
#define CARD_LINE_WIDTH     (CARD_BLOCK_SIZE * BYTES_PER_MAGNITUDE)
#define P_TAPE_LINE_WIDTH   (P_TAPE_BLOCK_SIZE * BYTES_PER_MAGNITUDE)
#define MAX_LINE_WIDTH      (PRINTER_LINE_WIDTH)

#define INTERLOCK_TIME      25
#define NOP_TIME            1
#define ADD_SUB_TIME        2
#define MUL_TIME            10
#define DIV_TIME            12
#define SPECIAL_TIME        1
#define SHIFT_TIME          2
#define BASE_MOVE_TIME      1
#define LOAD_STORE_TIME     2
#define IO_TIME             (1 + INTERLOCK_TIME)
#define JUMP_TIME           1
#define IMMEDIATE_TIME      1
#define COMPARE_TIME        2

#define MOVE_OPCODE         7

#endif // MIX_DEFS_H
