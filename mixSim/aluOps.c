/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
05mar23 m_c  fix comment
14jul20 m_c  initial version
*/

/*
 * Description: Implementation of arithmetic and shift operations.
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */

#include <stdint.h>
#include <stdbool.h>

#include "mixDefs.h"
#include "mixTypes.h"
#include "mixDecode.h"
#include "mix.h"

/***************************************************************************************************
*
* sum - add or subtract to/from the A register
*
*/

static status_t sum         // OK if successful, error code otherwise, see memToReg()
(
    uint32_t    f,          // F part of the instruction
    uint32_t    i,          // I part of the instruction
    int32_t     addr,       // signed A part of the instruction
    bool        negate      // true: subtract, false: add
)
{
    uint32_t    m = memoryIndex(addr, i);
    status_t    result = memToReg(&(memory[m]), &operand1, f, negate);

    if (result == OK)
    {
        int32_t memVal = mixIntTo2sC(&operand1);
        int32_t aVal = mixIntTo2sC(&A);
        twosCToMixInt((aVal + memVal), &A, MAGNITUDE_MASK);
    }

    return result;
}

/***************************************************************************************************
*
* add - implement the MIX add instruction
*
* Adds the value at memory location M to the A register.
*
*/

status_t add                // OK if successful, error code otherwise
(
    uint32_t    c,          // op code (unused)
    uint32_t    f,          // F part of the instruction
    uint32_t    i,          // I part of the instruction
    int32_t     addr        // signed A part of the instruction
)
{
    status_t result = OK;

    if (f == FADD_VARIANT)
    {
        result = UNIMPLEMENTED_F;       // floating point is not implemented
    }
    else
    {
        result = sum(f, i, addr, false); // negate flag indicates adition
    }

    return result;
}

/***************************************************************************************************
*
* sub - implement the MIX sub instruction
*
* Subtracts the value at memory location M from the A register.
*
*/

status_t sub                // OK if successful, error code otherwise
(
    uint32_t    c,          // op code (unused)
    uint32_t    f,          // F part of the instruction
    uint32_t    i,          // I part of the instruction
    int32_t     addr        // signed A part of the instruction
)
{
    status_t result = OK;

    if (f == FSUB_VARIANT)
    {
        result = UNIMPLEMENTED_F;       // floating point is not implemented
    }
    else
    {
        result = sum(f, i, addr, true); // negate flag indicates subtraction
    }

    return result;
}

/***************************************************************************************************
*
* mul - implement the MIX mul instruction
*
* Multiplies the value at memory location M by the A register and stores the product in AX.
*
*/

status_t mul                // OK if successful, error code otherwise
(
    uint32_t    c,          // op code (unused)
    uint32_t    f,          // F part of the instruction
    uint32_t    i,          // i part of the instruction
    int32_t     addr        // signed A part of the instruction
)
{
    uint32_t    m = memoryIndex(addr, i);
    status_t    result = memToReg(&(memory[m]), &operand1, f, false);

    if (result == OK)
    {
        int32_t     memVal = mixIntTo2sC(&operand1);
        int32_t     aVal = mixIntTo2sC(&A);
        int64_t     product = aVal * memVal;
        int32_t     sign = (product < 0) ? -1 : 1;
        uint64_t    absVal = (product < 0) ? -product : product;
        int32_t     x = sign * (absVal & MAGNITUDE_MASK);
        int32_t     a = sign * (absVal >> BITS_PER_MAGNITUDE);

        twosCToMixInt(x, &X, MAGNITUDE_MASK);
        twosCToMixInt(a, &A, MAGNITUDE_MASK);
    }

    return result;
}

/***************************************************************************************************
*
* mixdiv - implement the MIX div instruction
*
* Divides the AX register by the value at memory location M and stores the quotient in A and the
* remainder in X.
*
*/

status_t mixdiv             // OK if successful, error code otherwise
(
    uint32_t    c,          // op code (unused)
    uint32_t    f,          // F part of the instruction
    uint32_t    i,          // i part of the instruction
    int32_t     addr        // signed A part of the instruction
)
{
    uint32_t    m = memoryIndex(addr, i);
    status_t    result = memToReg(&(memory[m]), &operand1, f, false);

    if (result == OK)
    {
        int32_t     memVal = mixIntTo2sC(&operand1);
        uint32_t    aMag = A.integer.magnitude;

        if ((memVal != 0) || (aMag < memVal))
        {
            uint32_t    xMag = X.integer.magnitude;
            int32_t     sign = (A.integer.sign == 1) ? -1 : 1;
            int64_t     ax = sign * ((aMag << BITS_PER_MAGNITUDE) | xMag);
            int64_t     quotient = ax / memVal;
            int64_t     remainder = ax % memVal;

            twosCToMixInt(remainder, &X, MAGNITUDE_MASK);
            twosCToMixInt(quotient, &A, MAGNITUDE_MASK);
        }
        else
        {
            V = true;
        }
    }

    return result;
}

static uint8_t carry;

/***************************************************************************************************
*
* slReg - shift a MIX register left by m bytes, preserving the last byte carried out
*
*
*/

static void slReg
(
    mixData_t   *reg,       // register to shift
    uint32_t    m           // number of bytes to shift
)
{
    for (int i = 0; i < m; ++i)
    {
        carry = reg->bytes.byte1;
        reg->bytes.byte1 = reg->bytes.byte2;
        reg->bytes.byte2 = reg->bytes.byte3;
        reg->bytes.byte3 = reg->bytes.byte4;
        reg->bytes.byte4 = reg->bytes.byte5;
        reg->bytes.byte5 = 0;
    }
}

/***************************************************************************************************
*
* srReg - shift a MIX register right by m bytes, preserving the last byte carried out
*
*
*/

static void srReg
(
    mixData_t   *reg,       // register to shift
    uint32_t    m           // number of bytes to shift
)
{
    for (int i = 0; i < m; ++i)
    {
        carry = reg->bytes.byte5;
        reg->bytes.byte5 = reg->bytes.byte4;
        reg->bytes.byte4 = reg->bytes.byte3;
        reg->bytes.byte3 = reg->bytes.byte2;
        reg->bytes.byte2 = reg->bytes.byte1;
        reg->bytes.byte1 = 0;
    }
}

/***************************************************************************************************
*
* slax - shift AX left by m bytes
*
*/

static void slax
(
    uint32_t    m   // number of bytes to shift
)
{
    for (int i = 0; i < m; ++i)
    {
        slReg(&A, 1);
        slReg(&X, 1);
        A.bytes.byte5 = carry;
    }
}

/***************************************************************************************************
*
* srax - shift AX right by m bytes
*
*/

static void srax
(
    uint32_t    m   // number of bytes to shift
)
{
    for (int i = 0; i < m; ++i)
    {
        srReg(&X, 1);
        srReg(&A, 1);
        X.bytes.byte1 = carry;
    }
}

/***************************************************************************************************
*
* slc - circular shift (rotate) AX left by m bytes
*
*/

static void slc
(
    uint32_t    m   // number of bytes to shift
)
{
    for (int i = 0; i < m; ++i)
    {
        uint8_t firstCarry;

        slReg(&A, 1);
        firstCarry = carry;
        slReg(&X, 1);
        A.bytes.byte5 = carry;
        X.bytes.byte5 = firstCarry;
    }
}

/***************************************************************************************************
*
* src - circular shift (rotate) AX right by m bytes
*
*/

static void src
(
    uint32_t    m   // number of bytes to shift
)
{
    for (int i = 0; i < m; ++i)
    {
        uint8_t firstCarry;

        srReg(&X, 1);
        firstCarry = carry;
        srReg(&A, 1);
        X.bytes.byte1 = carry;
        A.bytes.byte1 = firstCarry;
    }
}

/***************************************************************************************************
*
* shift - implement the MIX shift command
*
*/

status_t shift          // OK if successful, error code otherwise
(
    uint32_t    c,      // op code (unused)
    uint32_t    f,      // F part of the instruction
    uint32_t    i,      // i part of the instruction
    int32_t     addr    // signed A part of the instruction
)
{
    status_t    result = OK;
    uint32_t    m = memoryIndex(addr, i); // m wraps forward and backward

    if (m != 0)
    {

        switch (f)
        {
            case SLA_VARIANT:
                if (m >= BYTES_PER_MAGNITUDE) A.integer.magnitude = 0;
                else slReg(&A, m);
                break;
            case SRA_VARIANT:
                if (m >= BYTES_PER_MAGNITUDE) A.integer.magnitude = 0;
                else srReg(&A, m);
                break;
            case SLAX_VARIANT:
                if (m >= (BYTES_PER_MAGNITUDE * 2))
                {
                    A.integer.magnitude = 0;
                    X.integer.magnitude = 0;
                }
                else slax(m);
                break;
            case SRAX_VARIANT:
                if (m >= (BYTES_PER_MAGNITUDE * 2))
                {
                    A.integer.magnitude = 0;
                    X.integer.magnitude = 0;
                }
                else srax(m);
                break;
            case SLC_VARIANT:
                slc(m % (BYTES_PER_MAGNITUDE * 2));
                break;
            case SRC_VARIANT:
                src(m % (BYTES_PER_MAGNITUDE * 2));
                break;
            default:
                result = BAD_F;
                break;
        }
    }

    return result;
}
