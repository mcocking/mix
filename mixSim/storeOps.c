/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/

/*
 * Description: Implements the MIX store operations.
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */

#include <stdint.h>

#include "mixDefs.h"
#include "mixTypes.h"
#include "mix.h"

/***************************************************************************************************
*
* sta - implement the MIX sta operation
*
* Store the value of the A register to the field of the word at the specified memory location.
*
*/

status_t sta            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // instruction F part, (field specifier)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (regToMem(&A, &(memory[m]), f));
}

/***************************************************************************************************
*
* stx - implement the MIX stx operation
*
* Store the value of the X register to the field of the word at the specified memory location.
*
*/

status_t stx            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // instruction F part, (field specifier)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (regToMem(&X, &(memory[m]), f));
}

/***************************************************************************************************
*
* st1 - implement the MIX st1 operation
*
* Store the value of the I1 register to the field of the word at the specified memory location.
*
*/

status_t st1            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // instruction F part, (field specifier)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (regToMem(&(I[1]), &(memory[m]), f));
}

/***************************************************************************************************
*
* st2 - implement the MIX st2 operation
*
* Store the value of the I2 register to the field of the word at the specified memory location.
*
*/

status_t st2            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // instruction F part, (field specifier)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (regToMem(&(I[2]), &(memory[m]), f));
}

/***************************************************************************************************
*
* st3 - implement the MIX st3 operation
*
* Store the value of the I3 register to the field of the word at the specified memory location.
*
*/

status_t st3            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // instruction F part, (field specifier)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (regToMem(&(I[3]), &(memory[m]), f));
}

/***************************************************************************************************
*
* st4 - implement the MIX st4 operation
*
* Store the value of the I4 register to the field of the word at the specified memory location.
*
*/

status_t st4            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // instruction F part, (field specifier)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (regToMem(&(I[4]), &(memory[m]), f));
}

/***************************************************************************************************
*
* st5 - implement the MIX st5 operation
*
* Store the value of the I5 register to the field of the word at the specified memory location.
*
*/

status_t st5            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // instruction F part, (field specifier)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (regToMem(&(I[5]), &(memory[m]), f));
}

/***************************************************************************************************
*
* st6 - implement the MIX st6 operation
*
* Store the value of the I6 register to the field of the word at the specified memory location.
*
*/

status_t st6            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // instruction F part, (field specifier)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (regToMem(&(I[6]), &(memory[m]), f));
}

/***************************************************************************************************
*
* stj - implement the MIX stj operation
*
* Store the value of the J register to the field of the word at the specified memory location.
*
*/

status_t stj            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // instruction F part, (field specifier)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m;
    status_t result = OK;

    if (((f & R_MASK) <= MAX_L_R_ADDR) &&
        (((f & R_MASK) >> BITS_PER_NIBBLE) <= MAX_L_R_ADDR))
    {
        m = memoryIndex(addr, i);
        result = regToMem(&J, &(memory[m]), f);
    }
    else
    {
        result = BAD_F;
    }

    return result;
}

/***************************************************************************************************
*
* stz - implement the MIX stz operation
*
* Store the value of the Z register to the field of the word at the specified memory location.
*
*/

status_t stz            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // instruction F part, (field specifier)
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (regToMem(&Z, &(memory[m]), f));
}



