/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/


/*
 * Description: Implementation of the MIX load operations including the load negative operations.
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */

#include <stdint.h>

#include "mix.h"

/***************************************************************************************************
*
* lda - load the A register with the contents of the memory location M
*
*/

status_t lda            // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &A, f, false)); // negate flag clear
}

/***************************************************************************************************
*
* ldx - load the X register with the contents of the memory location M
*
*/

status_t ldx            // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &X, f, false)); // negate flag clear
}

/***************************************************************************************************
*
* ld1 - load the I1 register with the contents of the memory location M
*
*/

status_t ld1            // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &(I[1]), f, false)); // negate flag clear
}

/***************************************************************************************************
*
* ld2 - load the I2 register with the contents of the memory location M
*
*/

status_t ld2            // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &(I[2]), f, false)); // negate flag clear
}

/***************************************************************************************************
*
* ld3 - load the I3 register with the contents of the memory location M
*
*/

status_t ld3            // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &(I[3]), f, false)); // negate flag clear
}

/***************************************************************************************************
*
* ld4 - load the I4 register with the contents of the memory location M
*
*/

status_t ld4            // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &(I[4]), f, false)); // negate flag clear
}

/***************************************************************************************************
*
* ld5 - load the I5 register with the contents of the memory location M
*
*/

status_t ld5            // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &(I[5]), f, false)); // negate flag clear
}

/***************************************************************************************************
*
* ld6 - load the I6 register with the contents of the memory location M
*
*/

status_t ld6            // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &(I[6]), f, false)); // negate flag clear
}

/***************************************************************************************************
*
* ldan - load the A register with the negative of the contents of the memory location M
*
*/

status_t ldan           // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &A, f, true)); // negate flag set
}

/***************************************************************************************************
*
* ldxn - load the X register with the negative of the contents of the memory location M
*
*/

status_t ldxn           // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &X, f, true)); // negate flag set
}

/***************************************************************************************************
*
* ld1n - load the I1 register with the negative of the  contents of the memory location M
*
*/

status_t ld1n           // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &(I[1]), f, true)); // negate flag set
}

/***************************************************************************************************
*
* ld2n - load the I2 register with the negative of the contents of the memory location M
*
*/

status_t ld2n           // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &(I[2]), f, true)); // negate flag set
}

/***************************************************************************************************
*
* ld3n - load the I3 register with the negative of the contents of the memory location M
*
*/

status_t ld3n           // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &(I[3]), f, true)); // negate flag set
}

/***************************************************************************************************
*
* ld4n - load the I4 register with the negative of the contents of the memory location M
*
*/

status_t ld4n           // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &(I[4]), f, true)); // negate flag set
}

/***************************************************************************************************
*
* ld5n - load the I5 register with the negative of the contents of the memory location M
*
*/

status_t ld5n           // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &(I[5]), f, true)); // negate flag set
}

/***************************************************************************************************
*
* ld6n - load the I6 register with the negative of the contents of the memory location M
*
*/

status_t ld6n           // OK if success, error code otherwise
(
    uint32_t    c,      // instruction opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    uint32_t m = memoryIndex(addr, i);

    return (memToReg(&(memory[m]), &(I[6]), f, true)); // negate flag set
}
