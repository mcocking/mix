/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/


#ifndef MIX_H
#define MIX_H

#include <stdbool.h>
#include <stdint.h>

#include "mixDefs.h"
#include "mixTypes.h"

// defined by the MIX architecture

extern mixWord_t memory[NUM_MEMORY_CELLS];
extern mixData_t A;
extern mixData_t X;
extern mixData_t I[NUM_I_REGS];
extern mixData_t J;

extern bool E;
extern bool L;
extern bool G;
extern bool V;

// hidden (not defined by the MIX actecture)

extern mixData_t operand1;
extern mixData_t operand2;
extern mixData_t Z;

extern uint32_t pc;
extern uint32_t time;
extern bool     running;

// MIX <-> ASCII

extern char mixToAsciiMap[];
extern char asciiToMixMap[];

// global function decls

extern void     mixInit(void);
extern status_t ioUnitInit(uint32_t, char *);
extern void     takeIoUnitsOffline(void);
extern void     displayIoUnits(void);
extern int32_t  mixIntTo2sC(const mixData_t *);
extern void     twosCToMixInt(int32_t, mixData_t *, uint32_t);
extern status_t splitF(uint32_t, uint32_t *, uint32_t *);
extern status_t memToReg(const mixWord_t *, mixData_t *, uint32_t, bool);
extern status_t regToMem(const mixData_t *, mixWord_t *, uint32_t);
extern uint32_t memoryIndex(int32_t, uint32_t);
extern int32_t  immediate(int32_t, uint32_t);
extern int32_t  address(mixInstr_t *);
extern status_t step(void);
extern char     *disasm(uint32_t);

#endif // MIX_H
