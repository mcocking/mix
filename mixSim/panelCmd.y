/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/

/*
 * Description: Parser for command interface.
 *
 */


%{

#include <stdio.h>

#include "controlPanel.h"
#include "break.h"
#include "mix.h"

extern int yylex(void);
extern int yyerror(char *s);

void help(void);

%}

%union
{
    char                *sym;
    char                s;
    unsigned long int   n;
}

%token B_CMD
%token BS_CMD
%token BR_CMD
%token BE_CMD
%token BD_CMD
%token C_CMD
%token D_CMD
%token G_CMD
%token H_CMD
%token M_CMD
%token P_CMD
%token Q_CMD
%token R_CMD
%token S_CMD
%token U_CMD
%token W_CMD

%token RANGE

%token <s> MINUS_SIGN
%token <s> PLUS_SIGN

%token <n> OCTAL_UNUM
%token <n> HEX_UNUM
%token <n> DEC_UNUM

%token <sym> FILE_NAME

%token EOL

%type <s>   sign
%type <n>   unum

%%

cmdlist: /* nothing */
  | cmdlist EOL             {panelPrompt();}
  | cmdlist cmd EOL         {panelPrompt();}
  | cmdlist error EOL       {panelPrompt(); yyerrok;}
  ;

cmd:
    B_CMD                   {displayBrkPts();}
  | BS_CMD unum             {setBrkPt($2);}
  | BR_CMD unum             {removeBrkPt($2);}
  | BE_CMD unum             {enableBrkPt($2);}
  | BD_CMD unum             {disableBrkPt($2);}
  | C_CMD                   {go(-1);}
  | D_CMD                   {displayMix(stepStatus);}
  | G_CMD unum              {go($2);}
  | H_CMD                   {help();}
  | M_CMD unum              {panelSetMem($2);}
  | M_CMD PLUS_SIGN unum    {panelSetMem($3);}
  | P_CMD unum              {panelSetPc($2);}
  | P_CMD PLUS_SIGN unum    {panelSetPc($3);}
  | Q_CMD                   {panelQuit();}
  | R_CMD unum              {panelSetMemWindow($2);}
  | R_CMD unum RANGE unum   {panelSetMemWinRange($2, $4);}
  | S_CMD                   {stepStatus = step(); displayMix(stepStatus);}
  | U_CMD unum FILE_NAME    {takeIoUnitOnline($2, $3);}
  | W_CMD sign unum         {panelLoadWord($2, $3);}
  ;

sign: MINUS_SIGN | PLUS_SIGN;

unum: OCTAL_UNUM | HEX_UNUM | DEC_UNUM;

%%

void help(void)
{
printf("\n%s to break running MIX program\n", getBreakHelp());
printf("b              show breakpoints\n");
printf("bs <address>   set breakpoint at <address>\n");
printf("br <address>   remove breakpoint at <address>\n");
printf("be <address>   enable breakpoint at <address>\n");
printf("bd <address>   disable breakpoint at <address>\n");
printf("c              continue (start running at current PC)\n");
printf("d              display MIX status, flags, register, PC, memory\n");
printf("g  <address>   start running with PC = <address>\n");
printf("h              display this help info\n");
printf("m  <address>   display/load at <address>\n");
printf("p  <address>   set PC = <address>\n");
printf("q              quit MIX simulator\n");
printf("r  <n>         set number of line of memory to display\n");
printf("r  <s> to <e>  set range of memory addresses to display\n");
printf("s              single step\n");
printf("u  <n> <file>  assign file as IO unit\n");
printf("w  (+|-) <30-bit unsigned word>  load word at current address\n");
}
