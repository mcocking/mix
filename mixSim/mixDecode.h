/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/

#ifndef MIX_DECODE_H
#define MIX_DECODE_H

#include <stdint.h>

#include "mixTypes.h"

// opcode values

#define NOP_OP              0
#define ADD_OP              1
#define SUB_OP              2
#define MUL_OP              3
#define DIV_OP              4
#define SPECIAL_OP          5
#define SHIFT_OP            6
#define MOVE_OP             7
#define LDA_OP              8
#define LD1_OP              9
#define LD2_OP              10
#define LD3_OP              11
#define LD4_OP              12
#define LD5_OP              13
#define LD6_OP              14
#define LDX_OP              15
#define LDAN_OP             16
#define LD1N_OP             17
#define LD2N_OP             18
#define LD3N_OP             19
#define LD4N_OP             20
#define LD5N_OP             21
#define LD6N_OP             22
#define LDXN_OP             23
#define STA_OP              24
#define ST1_OP              25
#define ST2_OP              26
#define ST3_OP              27
#define ST4_OP              28
#define ST5_OP              29
#define ST6_OP              30
#define STX_OP              31
#define STJ_OP              32
#define STZ_OP              33
#define JBUS_OP             34
#define IOC_OP              35
#define IN_OP               36
#define OUT_OP              37
#define JRED_OP             38
#define JMP_OP              39
#define JA_OP               40
#define J1_OP               41
#define J2_OP               42
#define J3_OP               43
#define J4_OP               44
#define J5_OP               45
#define J6_OP               46
#define JX_OP               47
#define IMMA_OP             48
#define IMM1_OP             49
#define IMM2_OP             50
#define IMM3_OP             51
#define IMM4_OP             52
#define IMM5_OP             53
#define IMM6_OP             54
#define IMMX_OP             55
#define CMPA_OP             56
#define CMP1_OP             57
#define CMP2_OP             58
#define CMP3_OP             59
#define CMP4_OP             60
#define CMP5_OP             61
#define CMP6_OP             62
#define CMPX_OP             63

// floating point variants

#define FLOAT_VARIANT       6

// ADD opcode variants

#define FADD_VARIANT        6

// SUB opcode variants

#define FSUB_VARIANT        6

// MUL opcode variants

#define FMUL_VARIANT        6

// DIV opcode variants

#define FDIV_VARIANT        6

// Special opcode variants

#define NUM_VARIANT         0
#define CHAR_VARIANT        1
#define HLT_VARIANT         2

// Shift opcode variants

#define SLA_VARIANT         0
#define SRA_VARIANT         1
#define SLAX_VARIANT        2
#define SRAX_VARIANT        3
#define SLC_VARIANT         4
#define SRC_VARIANT         5

// Jump variants

#define JMP_VARIANT         0
#define JSJ_VARIANT         1
#define JOV_VARIANT         2
#define JNOV_VARIANT        3
#define JL_VARIANT          4
#define JE_VARIANT          5
#define JG_VARIANT          6
#define JGE_VARIANT         7
#define JNE_VARIANT         8
#define JLE_VARIANT         9

#define JN_VARIANT          0
#define JZ_VARIANT          1
#define JP_VARIANT          2
#define JNN_VARIANT         3
#define JNZ_VARIANT         4
#define JNP_VARIANT         5

// Address transfer variants

#define INC_VARIANT         0
#define DEC_VARIANT         1
#define ENT_VARIANT         2
#define ENN_VARIANT         3

// CMPA variants

#define FCMP_VARIANT        6

extern opTableEntry_t   opTable[NUM_OPCODES];
extern char             *statusStr[];

// opcode function declarations

extern status_t nop(uint32_t, uint32_t, uint32_t, int32_t);     // 00
extern status_t add(uint32_t, uint32_t, uint32_t, int32_t);     // 01
extern status_t sub(uint32_t, uint32_t, uint32_t, int32_t);     // 02
extern status_t mul(uint32_t, uint32_t, uint32_t, int32_t);     // 03
extern status_t mixdiv(uint32_t, uint32_t, uint32_t, int32_t);  // 04
extern status_t special(uint32_t, uint32_t, uint32_t, int32_t); // 05
extern status_t shift(uint32_t, uint32_t, uint32_t, int32_t);   // 06
extern status_t move(uint32_t, uint32_t, uint32_t, int32_t);    // 07
extern status_t lda(uint32_t, uint32_t, uint32_t, int32_t);     // 08
extern status_t ld1(uint32_t, uint32_t, uint32_t, int32_t);     // 09
extern status_t ld2(uint32_t, uint32_t, uint32_t, int32_t);     // 10
extern status_t ld3(uint32_t, uint32_t, uint32_t, int32_t);     // 11
extern status_t ld4(uint32_t, uint32_t, uint32_t, int32_t);     // 12
extern status_t ld5(uint32_t, uint32_t, uint32_t, int32_t);     // 13
extern status_t ld6(uint32_t, uint32_t, uint32_t, int32_t);     // 14
extern status_t ldx(uint32_t, uint32_t, uint32_t, int32_t);     // 15
extern status_t ldan(uint32_t, uint32_t, uint32_t, int32_t);    // 16
extern status_t ld1n(uint32_t, uint32_t, uint32_t, int32_t);    // 17
extern status_t ld2n(uint32_t, uint32_t, uint32_t, int32_t);    // 18
extern status_t ld3n(uint32_t, uint32_t, uint32_t, int32_t);    // 19
extern status_t ld4n(uint32_t, uint32_t, uint32_t, int32_t);    // 20
extern status_t ld5n(uint32_t, uint32_t, uint32_t, int32_t);    // 21
extern status_t ld6n(uint32_t, uint32_t, uint32_t, int32_t);    // 22
extern status_t ldxn(uint32_t, uint32_t, uint32_t, int32_t);    // 23
extern status_t sta(uint32_t, uint32_t, uint32_t, int32_t);     // 24
extern status_t st1(uint32_t, uint32_t, uint32_t, int32_t);     // 25
extern status_t st2(uint32_t, uint32_t, uint32_t, int32_t);     // 26
extern status_t st3(uint32_t, uint32_t, uint32_t, int32_t);     // 27
extern status_t st4(uint32_t, uint32_t, uint32_t, int32_t);     // 28
extern status_t st5(uint32_t, uint32_t, uint32_t, int32_t);     // 29
extern status_t st6(uint32_t, uint32_t, uint32_t, int32_t);     // 30
extern status_t stx(uint32_t, uint32_t, uint32_t, int32_t);     // 31
extern status_t stj(uint32_t, uint32_t, uint32_t, int32_t);     // 32
extern status_t stz(uint32_t, uint32_t, uint32_t, int32_t);     // 33
extern status_t jbus(uint32_t, uint32_t, uint32_t, int32_t);    // 34
extern status_t ioc(uint32_t, uint32_t, uint32_t, int32_t);     // 35
extern status_t in(uint32_t, uint32_t, uint32_t, int32_t);      // 36
extern status_t out(uint32_t, uint32_t, uint32_t, int32_t);     // 37
extern status_t jred(uint32_t, uint32_t, uint32_t, int32_t);    // 38
extern status_t jmp(uint32_t, uint32_t, uint32_t, int32_t);     // 39
extern status_t ja(uint32_t, uint32_t, uint32_t, int32_t);      // 40
extern status_t mixj1(uint32_t, uint32_t, uint32_t, int32_t);   // 41
extern status_t j2(uint32_t, uint32_t, uint32_t, int32_t);      // 42
extern status_t j3(uint32_t, uint32_t, uint32_t, int32_t);      // 43
extern status_t j4(uint32_t, uint32_t, uint32_t, int32_t);      // 44
extern status_t j5(uint32_t, uint32_t, uint32_t, int32_t);      // 45
extern status_t j6(uint32_t, uint32_t, uint32_t, int32_t);      // 46
extern status_t jx(uint32_t, uint32_t, uint32_t, int32_t);      // 47
extern status_t imma(uint32_t, uint32_t, uint32_t, int32_t);    // 48
extern status_t imm1(uint32_t, uint32_t, uint32_t, int32_t);    // 49
extern status_t imm2(uint32_t, uint32_t, uint32_t, int32_t);    // 50
extern status_t imm3(uint32_t, uint32_t, uint32_t, int32_t);    // 51
extern status_t imm4(uint32_t, uint32_t, uint32_t, int32_t);    // 52
extern status_t imm5(uint32_t, uint32_t, uint32_t, int32_t);    // 53
extern status_t imm6(uint32_t, uint32_t, uint32_t, int32_t);    // 54
extern status_t immx(uint32_t, uint32_t, uint32_t, int32_t);    // 55
extern status_t cmpa(uint32_t, uint32_t, uint32_t, int32_t);    // 56
extern status_t cmp1(uint32_t, uint32_t, uint32_t, int32_t);    // 57
extern status_t cmp2(uint32_t, uint32_t, uint32_t, int32_t);    // 58
extern status_t cmp3(uint32_t, uint32_t, uint32_t, int32_t);    // 59
extern status_t cmp4(uint32_t, uint32_t, uint32_t, int32_t);    // 60
extern status_t cmp5(uint32_t, uint32_t, uint32_t, int32_t);    // 61
extern status_t cmp6(uint32_t, uint32_t, uint32_t, int32_t);    // 62
extern status_t cmpx(uint32_t, uint32_t, uint32_t, int32_t);    // 63

#endif // MIX_DECODE_H
