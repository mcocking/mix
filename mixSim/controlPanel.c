/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
26feb23 m_c  fixed compiler warnings
14jul20 m_c  initial version
*/


/*
 * Description: Provide a command line front end to the MIX simulator. Calls into these routines
 * are made from the command scanner/parser written in flex/bison. This file contains the main
 * function for the MIX simulator executable. See help() in panelCmd.y for a list of all the
 * supported commands.
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>
#include <locale.h>

#include "mixDefs.h"
#include "mixTypes.h"
#include "mixDecode.h"
#include "controlPanel.h"
#include "break.h"
#include "mix.h"

#include "../include/symTab.h"

static char         buf[TMP_BUF_SZ] = {'\0'};
static uint32_t     memIndex;
static uint32_t     memWindow;
static bool         interactive;

// number of entries in the breakpoint table

#define NHASH       9997

// used to keep track of breakpoints, keys are addresses and values indicate enabled/disabled

symTab_t    breakPtTab;

// indicates status of the last operation executed

status_t    stepStatus = OK;

// interface to the parser

extern int yyparse(void);
extern int yyrestart(FILE *);

/***************************************************************************************************
*
* charCodes - translate a 5 byte MIX string to ASCII
*
* The 5 MIX characters are contained in a single MIX word. The output string is not NULL
* terminated.
*
*/

static void charCodes
(
    mixData_t   *word,      // MIX word containing the 5 MIX characters
    char        *string     // output: ASCII string
)
{
    string[0] = mixToAsciiMap[word->bytes.byte1];
    string[1] = mixToAsciiMap[word->bytes.byte2];
    string[2] = mixToAsciiMap[word->bytes.byte3];
    string[3] = mixToAsciiMap[word->bytes.byte4];
    string[4] = mixToAsciiMap[word->bytes.byte5];
}

/***************************************************************************************************
*
* displayWord - print the value of a MIX word to stdout
*
* The value of the word is displayed in octal, decimal and as a 5 byte MIX string.
*
*/

static void displayWord
(
    char        *label,     // a text label to preceed the value
    mixData_t   *word       // the MIX word to display
)
{
    char alpha[BYTES_PER_MAGNITUDE + 1];
    char sign = (word->bytes.sign == 0) ? '+' :'-';

    charCodes(word, alpha);
    printf("%s: %c %02o,%02o,%02o,%02o,%02o (%010d.%s)\t",
           label, sign,
           word->bytes.byte1,
           word->bytes.byte2,
           word->bytes.byte3,
           word->bytes.byte4,
           word->bytes.byte5,
           word->integer.magnitude,
           alpha);
}

/***************************************************************************************************
*
* displayMix - print the state of the MIX machine to stdout
*
* All MIX registers, PC, flags, a small window of memory and mounted IO units are displayed.
*
*/

void displayMix
(
    status_t status     // status of last step
)
{
    char        s[32];
    uint32_t    index;

    // display flags and accumulated run time

    printf("\nMIX: %s\t\tFLAGS: ", statusStr[status]);
    if (V == true) printf("V "); else printf("_ ");
    if (L == true) printf("L"); else printf("_");
    if (E == true) printf("E"); else printf("_");
    if (G == true) printf("G"); else printf("_");
    printf("    TIME: %'d", time);
    printf("\n\n");

    // display registers

    displayWord("A ", &A); displayWord("X ", &X); printf("\n");
    displayWord("I1", &(I[1])); displayWord("I2", &(I[2])); printf("\n");
    displayWord("I3", &(I[3])); displayWord("I4", &(I[4])); printf("\n");
    displayWord("I5", &(I[5])); displayWord("I6", &(I[6])); printf("\n");
    displayWord("J ", &J);
    printf("\n\n");

    // display program counter

    sprintf(s, "PC:\n%010d", pc);
    displayWord(s, &(memory[pc].data));
    printf("\t%s", disasm(pc));
    printf("\n\n");

    // display memory

    printf("MEMORY:\n");
    for (int i = 0; i < memWindow; ++i)
    {
        index = (memIndex + i) & MAX_MEMORY_CELL;
        sprintf(s, "%010d", (index));
        displayWord(s, &(memory[index].data));
        printf("\t%s", disasm(index));
        printf("\n");
    }

    // display IO units

    displayIoUnits();

    printf("\n");
}

/***************************************************************************************************
*
* controlPanelInit - initialize the control panel's variables
*
*/

void controlPanelInit(void)
{
    memIndex = 0;
    memWindow = 10;
    interactive = false;
    running = false;
}

/***************************************************************************************************
*
* panelPrompt - print a command prompt to stdout
*
*/

void panelPrompt(void)
{
    if (interactive == true) printf("\nMIX> ");
}

/***************************************************************************************************
*
* hitBrkPt - check to see if the address is in the breakpoint table and is enabled
*
*/

bool hitBrkPt               // true if address is found and enabled, false otherwise
(
    uint32_t address        // address to check
)
{
    bool            result = false;
    symTabEntry_t   *entry;

    sprintf(buf, "%d", address);
    entry = lookUp(buf, &breakPtTab);
    if (entry != NULL)
    {
        if (entry->value == 1)
        {
            result = true;
        }
    }

    return result;
}

/***************************************************************************************************
*
* displayBrkPts - implement the [Bb] command
*
* Display the state of all breakpoints in the breakpoint table.
*
*/

void displayBrkPts(void)
{
    printTable("Breakpoints: 1 - enabled, 0 - disabled", "", &breakPtTab);
    printf("\n");
}

/***************************************************************************************************
*
* setBrkPt - implement the [Bb][Ss] command
*
* Add a new breakpoint to the breakpoint table. Make it enabled by default.
*
*/

void setBrkPt
(
    uint32_t address        // address of the new breakpoint
)
{
    symTabEntry_t *entry;

    sprintf(buf, "%d", address);
    entry = lookUp(buf, &breakPtTab);
    if (entry == NULL)
    {
        addEntry(buf, 1, &breakPtTab);
    }
}

/***************************************************************************************************
*
* removeBrkPt - implement the [Bb][Rr] command
*
* Remove a breakpoint from the breakpoint table.
*
*/

void removeBrkPt
(
    uint32_t address        // address of the breakpoint to be removed
)
{
    symTabEntry_t *entry;

    sprintf(buf, "%d", address);
    entry = lookUp(buf, &breakPtTab);
    if (entry != NULL)
    {
        deleteEntry(buf, &breakPtTab);
    }
}

/***************************************************************************************************
*
* enableBrkPt - implement the [Bb][Ee] command
*
* Enable a breakpoint in the breakpoint table. The breakpoint must already be in the table.
* Otherwise, no action is taken.
*
*/

void enableBrkPt
(
    uint32_t address        // address of the breakpoint to enable
)
{
    symTabEntry_t *entry;

    sprintf(buf, "%d", address);
    entry = lookUp(buf, &breakPtTab);
    if (entry != NULL)
    {
        entry->value = 1;
    }
}

/***************************************************************************************************
*
* disableBrkPt - implement the [Bb][Dd] command
*
* Disable a breakpoint in the breakpoint table. The breakpoint must already be in the table.
* Otherwise, no action is taken.

*
*/

void disableBrkPt
(
    uint32_t address        // address of the breakpoint to disable
)
{
    symTabEntry_t *entry;

    sprintf(buf, "%d", address);
    entry = lookUp(buf, &breakPtTab);
    if (entry != NULL)
    {
        entry->value = 0;
    }
}

/***************************************************************************************************
*
* go - implement the [Gg] command
*
* Step through the code until one or more of the following conditions is met: execution of a MIX
* operation returns with an error; the user sends a break (terminal stop); a breakpoint is hit.
*
*/

void go
(
    int index       // index into the memory array at which to start execution
)
{
    bool usrBrk;
    bool brkPt;

    if ((index >= 0) && (index < NUM_MEMORY_CELLS)) pc = index; // set the new value of the PC

    running = true;
    while (running == true)
    {
        stepStatus = step();
        usrBrk = gotBreak();
        brkPt = hitBrkPt(pc);
        if ((stepStatus != OK) || usrBrk || brkPt)
        {
            running = false;
        }
    }

    panelSetMem((pc - 1) & MAX_MEMORY_CELL);
    displayMix(stepStatus);
}

/***************************************************************************************************
*
* panelSetMem - implement the [Mm] command
*
* Set the memory location at which to load or display MIX words.
*
*/

void panelSetMem
(
    uint32_t newMemIndex        // index into the memory array
)
{
    memIndex = newMemIndex & MAX_MEMORY_CELL;     // memory wraps
}

/***************************************************************************************************
*
* panelSetPc - implment the [Pp] command
*
* Set the new value of the program counter.
*
*/

void panelSetPc
(
    uint32_t newPC      // new value of the program counter
)
{
    pc = newPC & MAX_MEMORY_CELL;     // memory wraps
}

/***************************************************************************************************
*
* panelQuit - implement the [Qq] command
*
* Exit the MIX simulator.
*
*/

void panelQuit(void)
{
    takeIoUnitsOffline();       // unmounts all IO units
    exit(0);
}

/***************************************************************************************************
*
* panelSetMemWindow - implements the [Rr] <n> command
*
* Set the number of memory locations to display.
*
*/

void panelSetMemWindow
(
    uint32_t newMemWindow       // set the size of the new memory window for display
)
{
    memWindow = newMemWindow;
}

/***************************************************************************************************
*
* panelSetMemWindow - implements the [Rr] <s> to <e> command
*
* Set the number of memory locations to display using a start/end pair of locations.
*
*/

void panelSetMemWinRange
(
    uint32_t start,     // start address of memory range
    uint32_t end        // end address of memory range
)
{
    uint32_t memRange = (start >= end) ? 1 : (end - start + 1);

    panelSetMemWindow(memRange);
    panelSetMem(start);
}

/***************************************************************************************************
*
* takeIoUnitOnline - implement the [Uu] command
*
*
* IO units are implemented with files. Specific devices are are assigned unit numbers as per
* the MIX architecture.
* See The Art of Computer Programming, 2nd Edition - Donald E. Knuth
*
*/

void takeIoUnitOnline
(
    uint32_t unitNumber,        // the unit number to "mount"
    char    *fileName           // the file to be used for IO
)
{
    status_t status = ioUnitInit(unitNumber, fileName);
    if (status != OK)
    {
        printf("\nError: could not open %s\n\n", fileName);
        panelPrompt();
    }
}

/***************************************************************************************************
*
* panelLoadWord - implement the [Ww] command
*
* Load a MIX word into memory at the current memory index. Use a separate sign and magitude
* to allow loading -0.
*
*/

void panelLoadWord
(
    char        sign,       // sign of the word: '-' or '+'
    uint32_t    magnitude   // absolute value of the word
)
{
    memory[memIndex].data.integer.sign = (sign == '-') ? 1 : 0;
    memory[memIndex].data.integer.magnitude = magnitude;
    memIndex++;
}

/***************************************************************************************************
*
* yyerror - parser's error reporting function
*
* This replaces the default bison error reporting function.
*
*/

int yyerror     // return value is unused
(
    char *s     // the error string
)
{
    fprintf(stderr, "error: %s\n", s);
    return 0;
}

/***************************************************************************************************
*
* main - the MIX simulator's entry point
*
* The simulator parses the specified assembled MIXAL program then enters ineractive mode.
*
*/

int main
(
    int     argc,
    char    *argv[]
)
{
    setlocale(LC_NUMERIC, "");      // allow "%'d" format with printf
    controlPanelInit();
    mixInit();
    breakInit();
    symTabInit(&breakPtTab, NHASH);

    if (argc > 1)
    {
        FILE *f = fopen(argv[1], "r");                      // load the assembled MIXAL file

        if (f != NULL)
        {
            printf("\nMIX: loading %s\n", argv[1]);
            yyrestart(f);
            yyparse();
        }
        else
        {
            printf("\nMIX: failed to load %s\n", argv[1]);
        }
    }

    memIndex = pc;
    interactive = true;
    displayMix(stepStatus);
    panelPrompt();

    yyrestart(stdin);           // restart the parser to enter interactive mode
    yyparse();

    symTabFree(&breakPtTab);
    return 0;
}
