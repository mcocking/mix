/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
05mar23 m_c  improved white space matching
04mar23 m_c  change comment character to '#'
26feb23 m_c  added %option noyywrap
14jul20 m_c  initial version
*/

%option noyywrap

/*
 * Description: Token scanner for command interface.
 *
 */


%{

#include "panelCmd.tab.h"

%}

%%
[Bb]            {return B_CMD;}
[Bb][Ss]        {return BS_CMD;}
[Bb][Rr]        {return BR_CMD;}
[Bb][Ee]        {return BE_CMD;}
[Bb][Dd]        {return BD_CMD;}
[Cc]            {return C_CMD;}
[Dd]            {return D_CMD;}
[Gg]            {return G_CMD;}
[Hh]            {return H_CMD;}
[Mm]            {return M_CMD;}
[Pp]            {return P_CMD;}
[Qq]            {return Q_CMD;}
[Rr]            {return R_CMD;}
[Ss]            {return S_CMD;}
[Uu]            {return U_CMD;}
[Ww]            {return W_CMD;}

[Tt][Oo]        {return RANGE;}

"-"             {yylval.s = yytext[0]; return MINUS_SIGN;}
"+"             {yylval.s = yytext[0]; return PLUS_SIGN;}

0[0-7]+         {yylval.n = strtoul(yytext, NULL, 0); return OCTAL_UNUM;}
0x[0-9a-fA-F]+  {yylval.n = strtoul(yytext, NULL, 0); return HEX_UNUM;}
0X[0-9a-fA-F]+  {yylval.n = strtoul(yytext, NULL, 0); return HEX_UNUM;}
(0|[1-9][0-9]*) {yylval.n = strtoul(yytext, NULL, 0); return DEC_UNUM;}

[a-zA-Z][-a-zA-Z_.0-9]*      {yylval.sym = strdup(yytext); return FILE_NAME;}

\n              {return EOL;}

[ \t\v\f\r]+            { /* ignore whitespace */ }
^[ \t\v\f\r]*"#".*$     { /* ignore comment */ }
^[ \t\v\f\r]*"*".*$     { /* ignore comment */ }
.                       { /* ignore extraneous characters */ }

%%
