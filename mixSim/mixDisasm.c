/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/

/*
 * Description: Disassemble MIX instruction word.
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdint.h>

#include "mixDefs.h"
#include "mixTypes.h"
#include "mixDecode.h"
#include "mix.h"

static char mixalStr[TMP_BUF_SZ];

// MIX opcode name strings

static char *opStr[] =
{
    "NOP",                  // 00
    "ADD",                  // 01
    "SUB",                  // 02
    "MUL",                  // 03
    "DIV",                  // 04
    "SPECIAL",              // 05
    "SHIFT",                // 06
    "MOVE",                 // 07
    "LDA",                  // 08
    "LD1",                  // 09
    "LD2",                  // 10
    "LD3",                  // 11
    "LD4",                  // 12
    "LD5",                  // 13
    "LD6",                  // 14
    "LDX",                  // 15
    "LDAN",                 // 16
    "LD1N",                 // 17
    "LD2N",                 // 18
    "LD3N",                 // 19
    "LD4N",                 // 20
    "LD5N",                 // 21
    "LD6N",                 // 22
    "LDXN",                 // 23
    "STA",                  // 24
    "ST1",                  // 25
    "ST2",                  // 26
    "ST3",                  // 27
    "ST4",                  // 28
    "ST5",                  // 29
    "ST6",                  // 30
    "STX",                  // 31
    "STJ",                  // 32
    "STZ",                  // 33
    "JBUS",                 // 34
    "IOC",                  // 35
    "IN",                   // 36
    "OUT",                  // 37
    "JRED",                 // 38
    "JMP",                  // 39
    "JA",                   // 40
    "J1",                   // 41
    "J2",                   // 42
    "J3",                   // 43
    "J4",                   // 44
    "J5",                   // 45
    "J6",                   // 46
    "JX",                   // 47
    "A",                    // 48
    "1",                    // 49
    "2",                    // 50
    "3",                    // 51
    "4",                    // 52
    "5",                    // 53
    "6",                    // 54
    "X",                    // 55
    "CMPA",                 // 56
    "CMP1",                 // 57
    "CMP2",                 // 58
    "CMP3",                 // 59
    "CMP4",                 // 60
    "CMP5",                 // 61
    "CMP6",                 // 62
    "CMPX"                  // 63
};

// MIX opcode variant name strings

static char *specialStr[] =
{
    "NUM",                  // 0
    "CHAR",                 // 1
    "HLT"                   // 2
};

static char *shiftStr[] =
{
    "SLA",                  // 0
    "SRA",                  // 1
    "SLAX",                 // 2
    "SRAX",                 // 3
    "SLC",                  // 4
    "SRC"                   // 5
};

static char *jmpStr[] =
{
    "JMP",                  // 0
    "JSJ",                  // 1
    "JOV",                  // 2
    "JNOV",                 // 3
    "JL",                   // 4
    "JE",                   // 5
    "JG",                   // 6
    "JGE",                  // 7
    "JNE",                  // 8
    "JLE"                   // 9
};

static char *jmpRegSuffixStr[] =
{
    "N",                    // 0
    "Z",                    // 1
    "P",                    // 2
    "NN",                   // 3
    "NZ",                   // 4
    "NP"                    // 5
};

static char *immRegPrefixStr[] =
{
    "INC",                  // 0
    "DEC",                  // 1
    "ENT",                  // 2
    "ENN"                   // 3
};

/***************************************************************************************************
*
* sprintfOpCode - match a name string to an opcode and variant specifier
*
*/

static int sprintOpcode     // length of matched name string
(
    uint8_t     c,          // instruction opcode
    uint32_t    f           // instruction F part (variant specifier)
)
{
    mixalStr[0] = '\0';

    if (c >= NUM_OPCODES)
    {
        sprintf(mixalStr, "?");
    }
    else if (((c >= ADD_OP) && (c <= DIV_OP)) || (c == CMPA_OP))
    {
        if (f == FLOAT_VARIANT) sprintf(mixalStr, "%c", 'F');
        int i = strlen(mixalStr);
        sprintf(&(mixalStr[i]), "%s", opStr[c]);
    }
    else if (c == SPECIAL_OP)
    {
        if (f <= HLT_VARIANT) sprintf(mixalStr, "%s", specialStr[f]);
        else sprintf(mixalStr, "?");
    }
    else if (c == SHIFT_OP)
    {
        if (f <= SRC_VARIANT) sprintf(mixalStr, "%s", shiftStr[f]);
        else sprintf(mixalStr, "?");
    }
    else if (c == JMP_OP)
    {
        if (f <= JLE_VARIANT) sprintf(mixalStr, "%s", jmpStr[f]);
        else sprintf(mixalStr, "?");
    }
    else if ((c >= JA_OP) && (c <= JX_OP))
    {
        if (f <= JNP_VARIANT) 
        {
            sprintf(mixalStr, "%s", opStr[c]);
            int i = strlen(mixalStr);
            sprintf(&(mixalStr[i]), "%s", jmpRegSuffixStr[f]);
        }
        else sprintf(mixalStr, "?");
    }
    else if ((c >= IMMA_OP) && (c <= IMMX_OP))
    {
        if (f <= ENN_VARIANT) 
        {
            sprintf(mixalStr, "%s", immRegPrefixStr[f]);
            int i = strlen(mixalStr);
            sprintf(&(mixalStr[i]), "%s", opStr[c]);
        }
        else sprintf(mixalStr, "?");
    }
    else
    {
        sprintf(mixalStr, "%s", opStr[c]);
    }

    return strlen(mixalStr);
}

/***************************************************************************************************
*
* disasm - disasemble the instruction at a memory location
*
*/

char *disasm            // disasembled instruction string
(
    uint32_t memIndex   // memory index of instruction to disasmble
)
{
    uint32_t    c = memory[memIndex].instr.fields.c;
    uint32_t    f = memory[memIndex].instr.fields.f;
    uint32_t    l;
    uint32_t    r;
    uint32_t    i = memory[memIndex].instr.fields.i;
    int32_t     addr = address(&(memory[memIndex].instr));
    status_t    result = splitF(f, &l, &r);
    int         offset = sprintOpcode(c, f);
    char        *argStr = &(mixalStr[offset]);

    if (result == BAD_F)
    {
        sprintf(argStr, "\t%d,%d(%d)", addr, i, f);
    }
    else
    {
        sprintf(argStr, "\t%d,%d(%d:%d)", addr, i, l, r);
    }

    return mixalStr;
}
