/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
26feb23 m_c  fixed compiler warnings
14jul20 m_c  initial version
*/

/*
 * Description: Provide the basic operations for conversion between MIX sign/magnitude and
 * twos compliment format; byte addressing; address generation; word loads and stores.
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */


#include <stdint.h>
#include <stdbool.h>
#include <assert.h>

#include "mixDefs.h"
#include "mixTypes.h"
#include "mixDecode.h"
#include "mix.h"

// defined by the MIX architecture

mixWord_t memory[NUM_MEMORY_CELLS];
mixData_t A;
mixData_t X;
mixData_t I[NUM_I_REGS];
mixData_t J;

// flags and overflow toggle

bool E;     // equal to 0
bool L;     // less than 0
bool G;     // greater than 0
bool V;     // overflow

// hidden (not defined by the MIX actecture)

mixData_t operand1;
mixData_t operand2;
mixData_t Z;

uint32_t    pc;
uint32_t    time;
bool        running;

/***************************************************************************************************
*
* mixInit - initialize the state of the machine
*
*/

void mixInit(void)
{
    // zero out the memory and visible registers

    for (int i = 0; i < NUM_MEMORY_CELLS; ++i)
    {
        memory[i].bits = 0;
    }

    A.bits = 0;
    X.bits = 0;

    for (int i = 0; i < NUM_I_REGS; ++i)
    {
        I[i].bits = 0;
    }

    J.bits = 0;

    // initial conditions for the flags

    E = true;
    L = false;
    G = false;
    V = false;

    // hidden registers

    operand1.bits = 0;
    operand2.bits = 0;
    Z.bits = 0;

    // execution starts at 0 by default

    pc = 0;
    time = 0;
    running = false;
}

/***************************************************************************************************
*
* mixIntTo2sC - convert an interger from MIX sign/magnitude to twos compliment
*
*/

int32_t mixIntTo2sC             // twos compliment integer
(
    const mixData_t *mixInt     // MIX sign/magnitude integer
)
{
    // magnitude is 30 bits, so there's no loss of precision

    int32_t number = mixInt->integer.magnitude;

    return ((mixInt->integer.sign == 0) ? number : -number);
}


/***************************************************************************************************
*
* twosCToMixInt - convert an interger from twos compliment to MIX sign/magnitude
*
*/

void twosCToMixInt
(
    int32_t     number,     // twos compliment integer to convert
    mixData_t   *mixInt,    // output: MIX sign/magnitude integer
    uint32_t    mask        // max magnitude allowed
)
{
    if (number < 0)
    {
        mixInt->integer.magnitude = (-number & mask);
        mixInt->integer.sign = 1;
        if (-number > mask) V = 1;          // overflow in the negative direction
    }
    else if (number == 0)
    {
        // don't change existing sign

        mixInt->integer.magnitude = 0;
    }
    else
    {
        mixInt->integer.magnitude = (number & mask);
        mixInt->integer.sign = 0;
        if (number > mask) V = 1;           // overflow in the positive direction
    }
}

/***************************************************************************************************
*
* splitF - split a single valued F part into left and right values of the format: (l : r)
*
*/

status_t splitF         // OK if successful, error code otherwise
(
    uint32_t    f,      // F part
    uint32_t    *l,     // output: left value in (l : r)
    uint32_t    *r      // output: right value in (l : r)
)
{
    status_t    result = OK;

    *l = (f & L_MASK) >> BITS_PER_NIBBLE;
    *r = (f & R_MASK);

    if ( (*l > MAX_L_R) ||
         (*r > MAX_L_R) ||
         (*r < *l) )
    {
    result = BAD_F;
    }

    return result;
}

/***************************************************************************************************
*
* makeMagMask - construct a MIX word magnitude mask from an F part
*
* The mask is used to isolate the byte fields specified by an F part with the format (l : r).
* In addition, the number of bits needed to right justify the field in the destination register
* is returned.
*
*/

static uint32_t makeMagMask     // mask
(
    uint32_t    l,              // left value of F part with format (l : r)
    uint32_t    r,              // right value of F part with format (l : r)
    uint32_t    *shiftBits      // output: number of bits needed to right justify the field
)
{
    uint32_t mask = 0;
    uint32_t widthBytes = 1;

    assert(l <= r);

    widthBytes += (r - l);
    *shiftBits = (MAX_L_R - r) * BITS_PER_BYTE;

    for (int w = 0; w < widthBytes; ++w)
    {
        mask = (mask << BITS_PER_BYTE) | BYTE_MASK;
    }

    return (mask << *shiftBits);
}

/***************************************************************************************************
*
* memToReg - copy the field of a memory location to a given register
*
* The field is right justified in the destination register.
*
*/

status_t memToReg               // OK if successful, error code otherwise
(
    const mixWord_t *mem,       // source memory word
    mixData_t       *reg,       // destination register
    uint32_t        f,          // F part (field specifier)
    bool            negate      // flag to indicate that the negative of the field value is loaded
)
{
    uint32_t    l;
    uint32_t    r;
    uint32_t    shift;
    uint32_t    mask;
    status_t    result = OK;

    if ( (result = splitF(f, &l, &r)) == OK)
    {
        // sign

        if (l == 0)
        {
            reg->integer.sign = (negate == true) ? ~mem->data.integer.sign
                                                 : mem->data.integer.sign;
            l = 1;
        }
        else
        {
            reg->integer.sign = (negate = true) ? 1 : 0;
        }

        // magnitude if specified by F

        if (l <= r)
        {
            mask = makeMagMask(l, r, &shift);
            reg->integer.magnitude = (mem->data.integer.magnitude & mask) >>
                                     shift;
        }
    }

    return result;
}

/***************************************************************************************************
*
* regToMem - copy the value in a register to the specified field of the specified memory word
*
*
*/

status_t regToMem           // Ok if successful, error code otherwise
(
    const mixData_t *reg,   // source register
    mixWord_t       *mem,   // destination memory word
    uint32_t        f       // F part (field specifier)
)
{
    uint32_t    l;
    uint32_t    r;
    uint32_t    shift;
    uint32_t    mask;
    uint32_t    dstMag = mem->data.integer.magnitude;
    uint32_t    srcMag = reg->integer.magnitude;
    status_t    result = OK;

    if ( (result = splitF(f, &l, &r)) == OK)
    {
        // sign

        if (l == 0)
        {
            mem->data.integer.sign = reg->integer.sign;
            l = 1;
        }

        // magnitude if specified by F

        if (l <= r)
        {
            mask = makeMagMask(l, r, &shift);
            dstMag &= ~mask;
            srcMag &= (mask >> shift);
            mem->data.integer.magnitude = dstMag | (srcMag << shift);
        }
    }

    return result;
}

/***************************************************************************************************
*
* memoryIndex - generate a memory index from a signed address and an index register
*
* Memory wraps forwards and backwards, so there are no memory out-of-bounds errors.
*
*/

uint32_t memoryIndex        // memory index between 0 and end of memory inclusive
(
    int32_t     addr,       // signed address
    uint32_t    i           // index register
)
{
    int32_t iVal;

    iVal = I[i].integer.magnitude;                  // no loss of precision possible
    if (I[i].integer.sign == 1) iVal = -iVal;

    return ((addr + iVal) & MAX_MEMORY_CELL);       // memory wraps
}

/***************************************************************************************************
*
* immediate - generate an immediate value from a signed address and an index register
*
* Used for address transfer operations.
*
*/

int32_t immediate       // immediate value, negative values allowed
(
    int32_t     addr,   // signed address
    uint32_t    i       // index register
)
{
    int32_t iVal;

    iVal = I[i].integer.magnitude;              // no loss of precision possible
    if (I[i].integer.sign == 1) iVal = -iVal;

    return (addr + iVal);                       // negative values allowed
}

/***************************************************************************************************
*
* address - generate a signed address from an instruction's A part and sign
*
*/

int32_t address         // signed address
(
    mixInstr_t *inst    // instruction word
)
{
    int32_t a = inst->fields.a;                 // a is 12 bits so no loss of precision
    return (inst->fields.sign == 0) ? a : -a;
}

/***************************************************************************************************
*
* step - single step the instruction pointed to by the program counter
*
*/

status_t step(void)         // OK if operation completed successfully, error code otherwise
{
    uint32_t    c;
    uint32_t    f;
    uint32_t    i;
    int32_t     addr;
    status_t    status;

    // make sure the state of the machine is correct

    assert(((E == true) && (L == false) && (G == false)) ||
           ((E == false) && (L == true) && (G == false)) ||
           ((E == false) && (L == false) && (G == true)));

    assert(I[0].bits == 0);
    for (int i = 1; i < NUM_I_REGS; ++i)
    {
    assert((I[i].bytes.byte1 == 0) && (I[i].bytes.byte2 == 0) &&
           (I[i].bytes.byte3 == 0));
    }

    assert((J.bytes.sign == 0) && (J.bytes.byte1 == 0) &&
           (J.bytes.byte2 == 0) && (J.bytes.byte3 == 0));
    assert(Z.bits == 0);

    // extract instruction fields (fetch instruction)

    c = memory[pc].instr.fields.c;
    f = memory[pc].instr.fields.f;
    i = memory[pc].instr.fields.i;
    addr = address(&(memory[pc].instr));
    status = BAD_I;

    pc = (pc + 1) & MAX_MEMORY_CELL;                    // memory wraps

    if (i < NUM_I_REGS)
    {
        status = ((opTable[c].op)(c, f, i, addr));  //execute instruction
        time += opTable[c].time;
    }
    else
    {
        status = BAD_I;
    }
    return status;
}
