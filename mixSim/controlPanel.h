/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/


#ifndef CONTROL_PANEL_H
#define CONTROL_PANEL_H

#include <stdint.h>

#include "mixTypes.h"

extern status_t stepStatus;

extern void panelPrompt         (void);
extern void displayMix          (status_t);
extern void displayBrkPts       (void);
extern void setBrkPt            (uint32_t);
extern void removeBrkPt         (uint32_t);
extern void enableBrkPt         (uint32_t);
extern void disableBrkPt        (uint32_t);
extern void go                  (int);
extern void panelSetMem         (uint32_t);
extern void panelSetPc          (uint32_t);
extern void panelQuit           (void);
extern void panelSetMemWindow   (uint32_t);
extern void panelSetMemWinRange (uint32_t, uint32_t);
extern void takeIoUnitOnline    (uint32_t, char *);
extern void panelLoadWord       (char, uint32_t);

#endif // CONTROL_PANEL_H
