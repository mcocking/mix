/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/


#ifndef MIX_TYPES_H
#define MIX_TYPES_H

#include <stdint.h>

#include "mixDefs.h"

typedef enum status
{
    OK = 0,
    ERROR,                  // unspecified error
    UNIMPLEMENTED_C,        // unimplemented opcode
    UNIMPLEMENTED_F,        // unimplemented opcode variant
    BAD_F,                  // bad range or opcode variant
    BAD_I,                  // bad I register
    IO_UNIT_NOT_ONLINE      // disk not mounted, printer offline, etc
} status_t;

typedef union mixData
{
    struct b
    {
        uint32_t byte5  :BITS_PER_BYTE;
        uint32_t byte4  :BITS_PER_BYTE;
        uint32_t byte3  :BITS_PER_BYTE;
        uint32_t byte2  :BITS_PER_BYTE;
        uint32_t byte1  :BITS_PER_BYTE;
        uint32_t sign   :1;
        uint32_t unused :1;
    } bytes;
    struct i
    {
        uint32_t magnitude  :BITS_PER_BYTE * BYTES_PER_MAGNITUDE;
        uint32_t sign       :1;
        uint32_t unused     :1;
    } integer;
    uint32_t bits;
} mixData_t;

typedef union mixInstr
{
    struct flds
    {
        uint32_t c      :6;
        uint32_t f      :6;
        uint32_t i      :6;
        uint32_t a      :12;
        uint32_t sign   :1;
        uint32_t unused :1;
    } fields;
    uint32_t bits;
} mixInstr_t;

typedef union mixWord
{
    mixData_t   data;
    mixInstr_t  instr;
    uint32_t    bits;
} mixWord_t;

// status_t operation(c, f, i, addr)

typedef status_t (*op_t)(uint32_t, uint32_t, uint32_t, int32_t);
typedef struct opEntries
{
    op_t        op;
    uint32_t    time;
} opTableEntry_t;

#endif // MIX_TYPES_H
