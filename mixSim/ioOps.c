/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
04mar23 m_c  extend character codes to include '%' and '#'
26feb23 m_c  fixed compiler warnings
14jul20 m_c  initial version
*/

/*
 * Description: Implements the MIX IO operations. All IO units are emulated with files.
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <stdlib.h>

#include "mixDefs.h"
#include "mixDecode.h"
#include "mix.h"

// the list of file pointers for all IO units described in Knuth

static FILE *ioUnits[NUM_IO_UNITS] = {NULL};

char mixToAsciiMap[] =
{
    ' ', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 't', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'f', 'p', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.', ',', '(', ')', '+',
    '-', '*', '/', '=', '$', '<', '>', '@', ';', ':', '\'', '%', '#'
};

char asciiToMixMap[] =
{
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 // ascii 0-15
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 // ascii 16-31
0, 0, 0, 57, 49, 56, 0, 55, 42, 43, 46, 44, 41, 45, 40, 47,      // ascii 32-47
30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 54, 53, 50, 48, 51, 0,  // ascii 48-63
52, 1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16,          // ascii 64-79
17, 18, 19, 22, 23, 24, 25, 26, 27, 28, 29, 0, 0, 0, 0, 0,      // ascii 80-95
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 // ascii 96-111
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,                 // ascii 112-127
};

// temp buffer for all char based IO units

static char charIoLine[MAX_LINE_WIDTH + 1];

/***************************************************************************************************
*
* charOut - output a block of characters to a printer, card punch or paper tape
*
* The block of characters is located in memory as specified by the index register and signed
* address.
*
*/

static void charOut
(
    uint32_t    unitNumber,     // IO unit number of the device
    uint32_t    blockSize,      // block size of the device
    uint32_t    i,              // index register
    int32_t     addr            // signed address
)
{
    uint32_t    m = memoryIndex(addr, i);
    uint32_t    linePos = 0;

    for (uint32_t i = 0; i < blockSize; ++i)
    {
        charIoLine[linePos++] = mixToAsciiMap[memory[m].data.bytes.byte1];
        charIoLine[linePos++] = mixToAsciiMap[memory[m].data.bytes.byte2];
        charIoLine[linePos++] = mixToAsciiMap[memory[m].data.bytes.byte3];
        charIoLine[linePos++] = mixToAsciiMap[memory[m].data.bytes.byte4];
        charIoLine[linePos++] = mixToAsciiMap[memory[m].data.bytes.byte5];
        m = (m + 1) & MAX_MEMORY_CELL;
    }
    charIoLine[linePos] = '\0';
    fprintf(ioUnits[unitNumber], "%s\n", charIoLine);
    fflush(ioUnits[unitNumber]);
}

/***************************************************************************************************
*
* charIn - read in a block of characters from a card reader or paper tape
*
* The block of characters is stored in memory as specified by the index register and signed
* address.
*
*/

static void charIn
(
    uint32_t    unitNumber,     // IO unit number of the device
    uint32_t    blockSize,      // block size of the device
    uint32_t    i,              // index register
    int32_t     addr            // signed address
)
{
    uint32_t    m = memoryIndex(addr, i);
    char        *inputLine = NULL;
    size_t      len = 0;
    uint32_t    linePos = 0;
    uint32_t    numChars;

    numChars = getline(&inputLine, &len, ioUnits[unitNumber]);
    strncpy(charIoLine, inputLine, numChars);
    free(inputLine);  // scrap any extra characters if the line is too long

    // pad the line with spaces if it's too short

    while (numChars < (blockSize * BYTES_PER_MAGNITUDE))
    {
        charIoLine[numChars++] = 0;
    }

    for (uint32_t i = 0; i < blockSize; ++i)
    {
        memory[m].data.bytes.sign = 0;
        memory[m].data.bytes.byte1 = asciiToMixMap[charIoLine[linePos++]];
        memory[m].data.bytes.byte2 = asciiToMixMap[charIoLine[linePos++]];
        memory[m].data.bytes.byte3 = asciiToMixMap[charIoLine[linePos++]];
        memory[m].data.bytes.byte4 = asciiToMixMap[charIoLine[linePos++]];
        memory[m].data.bytes.byte5 = asciiToMixMap[charIoLine[linePos++]];
        m = (m + 1) & MAX_MEMORY_CELL;
    }
}

/***************************************************************************************************
*
* jbus - implement the MIX jbus operation
*
* As long as the file corresponding to the IO unit is open, the unit is never busy.
*
*/

status_t jbus           // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction op code
    uint32_t    f,      // instruction F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    status_t result = OK;

    if (ioUnits[f] == NULL)
    {
        result = jmp(c, f, i, addr);
    }
    return result;
}

/***************************************************************************************************
*
* ioc - implement the MIX ioc operation
*
* The IO control operation for each unit is defined in Knuth.
*
*/

status_t ioc            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction op code
    uint32_t    f,      // instruction F part, specifies the IO unit number
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    status_t result = UNIMPLEMENTED_F;

    if (f < NUM_IO_UNITS)
    {
        switch (f)
        {
            case PRINTER:
                if (ioUnits[PRINTER] != NULL)
                {
                    for (uint32_t i = 0; i < PRINTER_PAGE_HEIGHT; ++i)
                    {
                        fprintf(ioUnits[PRINTER], "\n");
                    }
                    fflush(ioUnits[PRINTER]);
                    result = OK;
                }
                else result = IO_UNIT_NOT_ONLINE;
                break;
            default:
                result = UNIMPLEMENTED_F;
                break;
        }
    }
    return result;
}

/***************************************************************************************************
*
* in - implements the MIX in operation
*
* Move one block of words from an IO unit to memory.
*
*/

status_t in             // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction op code
    uint32_t    f,      // instruction F part, specifies the IO unit number
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    status_t result = UNIMPLEMENTED_F;

    if (f < NUM_IO_UNITS)
    {
        switch (f)
        {
            case CARD_READER:
                if (ioUnits[CARD_READER] != NULL)
                {
                    charIn(CARD_READER, CARD_BLOCK_SIZE, i, addr);
                    result = OK;
                }
                break;
            default:
                result = UNIMPLEMENTED_F;
        }
    }
    return result;
}

/***************************************************************************************************
*
* out - implements the MIX out operation
*
* Move one block of words from memory to an IO unit.
*
*/

status_t out            // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction op code
    uint32_t    f,      // instruction F part, specifies the IO unit number
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    status_t result = UNIMPLEMENTED_F;

    if (f < NUM_IO_UNITS)
    {
        switch (f)
        {
            case CARD_PUNCH:
                if (ioUnits[CARD_PUNCH] != NULL)
                {
                    charIn(CARD_PUNCH, CARD_BLOCK_SIZE, i, addr);
                    result = OK;
                }
                break;
            case PRINTER:
                if (ioUnits[PRINTER] != NULL)
                {
                    charOut(PRINTER, PRINTER_BLOCK_SIZE, i, addr);
                    result = OK;
                }
                break;
            default:
                result = UNIMPLEMENTED_F;
        }
    }
    return result;
}

/***************************************************************************************************
*
* jred - implement the MIX jred operation
*
* Jump if the IO unit is ready. IO units are emulated with files so as long as a file is
* open the coresponding IO unit is always ready.
*
*/

status_t jred           // OK if successful, error code otherwise
(
    uint32_t    c,      // instruction op code
    uint32_t    f,      // instruction F part, specifies the IO unit number
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    status_t result = OK;

    if (ioUnits[f] != NULL)
    {
        result = jmp(c, f, i, addr);
    }
    return result;
}

/***************************************************************************************************
*
* ioUnitInit - initialize an IO unit by opening its coresponding file.
*
* MIX IO units are emulated with files. An IO unit is "mounted" by opening its coresponding
* file with the correct permissions.
*
*/

status_t ioUnitInit             // OK if successful, error code otherwise
(
    uint32_t    unitNumber,     // IO unit number to initialize
    char        *fileName       // IO unit's coresponding file to be opened
)
{
    FILE        *f = NULL;
    status_t    status = ERROR;

    if (unitNumber < NUM_IO_UNITS)
    {
        switch (unitNumber)
        {
        case MAG_TAPE_0:                    // O_RDWR
        case MAG_TAPE_1:
        case MAG_TAPE_2:
        case MAG_TAPE_3:
        case MAG_TAPE_4:
        case MAG_TAPE_5:
        case MAG_TAPE_6:
        case MAG_TAPE_7:
        case DISK_0:
        case DISK_1:
        case DISK_2:
        case DISK_3:
        case DISK_4:
        case DISK_5:
        case DISK_6:
        case DISK_7:
            f = fopen(fileName, "r+");
            break;

        case CARD_READER:                   // O_RDONLY
            f = fopen(fileName, "r");
            break;

        case CARD_PUNCH:                    // O_WRONLY | O_CREAT | O_TRUNC
        case PRINTER:
            f = fopen(fileName, "w");
            break;

        case PAPER_TAPE:                    // O_RDWR | O_CREAT | O_APPEND
            f = fopen(fileName, "a+");
            break;

        default:                            // defensive
            status = ERROR;
            break;
        }

        if (f != NULL)
        {
            if (ioUnits[unitNumber] != NULL)
            {
                fflush(ioUnits[unitNumber]);
                fclose(ioUnits[unitNumber]);    // close any previously opened file
            }

            ioUnits[unitNumber] = f;
            status = OK;
        }
    }
    return status;
}

/***************************************************************************************************
*
* takeIoUnitsOffLine - "unmount" all IO units
*
* MIX IO units are emulated with files. An IO unit is "unmounted" by closing its coresponding
* file.
*
*/

void takeIoUnitsOffline(void)
{
    for (uint32_t i = 0; i < NUM_IO_UNITS; ++i)
    {
        if (ioUnits[i] != NULL) fclose(ioUnits[i]);
    }
}

/***************************************************************************************************
*
* displayIoUnits - display all "mounted" IO units
*
*/

void displayIoUnits(void)
{
    printf("\nIO UNITS: ");
    for (uint32_t i = 0; i < NUM_IO_UNITS; ++i)
    {
        if (ioUnits[i] != NULL) printf("%02d ", i);
        else printf("__ ");
    }
    printf("\n");
}
