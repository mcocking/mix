/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
26feb23 m_c  fixed compiler warnings
14jul20 m_c  initial version
*/

/*
 * Description: Implements the MIX special and move operations.
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */

#include <stdint.h>
#include <assert.h>

#include "mixDefs.h"
#include "mixDecode.h"
#include "mix.h"

#define MIX_ZERO_CODE   30

/***************************************************************************************************
*
* numToCode - convert the numerical value of a digit to the MIX character code
*
*/

static inline uint8_t numToCode     // MIX character code
(
    uint8_t num                     // numerical value (0 - 9)
)
{
    assert (num <= 9);
    return (MIX_ZERO_CODE + num);
}

/***************************************************************************************************
*
* numToCode - convert the character code of a digit to its numerical value
*
*/

static inline uint8_t codeToNum     // numerical value
(
    uint8_t code                    // MIX chatacter code
)
{
    return (code % 10);             // the MIX spec allows any code%10 to be a digit code
}

/***************************************************************************************************
*
* nop - implements the MIX nop operation
*
* No operation.
*
*/

status_t nop            // always returns OK
(
    uint32_t    c,      // operation code, unused
    uint32_t    f,      // F part, unused
    uint32_t    i,      // index register, unused
    int32_t     addr    // insigned address, unused
)
{
    return OK;
}

/***************************************************************************************************
*
* number - convert the 10 digit alpha string in AX to its numerical value
*
* The value is placed in A.
*
*/

static status_t number(void)        // always returns OK
{
    uint64_t aValue = 0;

    aValue += codeToNum(A.bytes.byte1);
    aValue *= 10;
    aValue += codeToNum(A.bytes.byte2);
    aValue *= 10;
    aValue += codeToNum(A.bytes.byte3);
    aValue *= 10;
    aValue += codeToNum(A.bytes.byte4);
    aValue *= 10;
    aValue += codeToNum(A.bytes.byte5);
    aValue *= 10;
    aValue += codeToNum(X.bytes.byte1);
    aValue *= 10;
    aValue += codeToNum(X.bytes.byte2);
    aValue *= 10;
    aValue += codeToNum(X.bytes.byte3);
    aValue *= 10;
    aValue += codeToNum(X.bytes.byte4);
    aValue *= 10;
    aValue += codeToNum(X.bytes.byte5);

    A.integer.magnitude = (aValue & MAGNITUDE_MASK);

    return OK;
}

/***************************************************************************************************
*
* character - converts the value in A to an alpha string
*
* The string is placed in AX.
*
*/

static status_t character(void)             // always returns OK
{
    uint32_t aValue = A.integer.magnitude;

    X.bytes.byte5 = numToCode(aValue % 10);
    aValue /= 10;
    X.bytes.byte4 = numToCode(aValue % 10);
    aValue /= 10;
    X.bytes.byte3 = numToCode(aValue % 10);
    aValue /= 10;
    X.bytes.byte2 = numToCode(aValue % 10);
    aValue /= 10;
    X.bytes.byte1 = numToCode(aValue % 10);
    aValue /= 10;
    A.bytes.byte5 = numToCode(aValue % 10);
    aValue /= 10;
    A.bytes.byte4 = numToCode(aValue % 10);
    aValue /= 10;
    A.bytes.byte3 = numToCode(aValue % 10);
    aValue /= 10;
    A.bytes.byte2 = numToCode(aValue % 10);
    aValue /= 10;
    A.bytes.byte1 = numToCode(aValue % 10);

    return OK;
}

/***************************************************************************************************
*
* hlt - halts the MIX machine
*
*/

static status_t hlt(void)       // always returns OK
{
    running = false;
    return OK;
}

/***************************************************************************************************
*
* special - implements the MIX special operation
*
* The special operation variants are one of: number, character or hlt.
*
*/

status_t special        // returns OK if successful, error code otherwise
(
    uint32_t    c,      // operation code, unused
    uint32_t    f,      // F part, used to select the operation variant
    uint32_t    i,      // index register, unused
    int32_t     addr    // signed address, unused
)
{
    status_t result = OK;

    switch (f)
    {
        case NUM_VARIANT:
            result = number();
            break;

        case CHAR_VARIANT:
            result = character();
            break;

        case HLT_VARIANT:
            result = hlt();
            break;

        default:
            result = BAD_F;
            break;
    }

    return result;
}

/***************************************************************************************************
*
* move - implements the MIX move operation
*
* Move f words from memory to memory. The source is at memory index M and the destination is at
* memory index I[1].
*
*/

status_t move               // always returns OK
(
    uint32_t    c,          // operation code, unused
    uint32_t    f,          // F part, number of words to move
    uint32_t    i,          // index register
    int32_t     addr        // signed address register
)
{
    uint32_t    srcIndex = memoryIndex(addr, i);
    uint32_t    dstIndex = I[1].integer.magnitude;

    for (int count = 0; count < f; ++count)
    {
        memory[dstIndex].bits = memory[srcIndex].bits;
        srcIndex = (srcIndex + 1) & MAX_MEMORY_CELL;
        dstIndex = (dstIndex + 1) & MAX_MEMORY_CELL;
    }

    I[1].integer.magnitude = (I[1].integer.magnitude + f) & MAX_MEMORY_CELL;

    time += (2 * f);        // time must be increased based on the number of words moved
    return OK;
}
