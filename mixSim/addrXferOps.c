/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/

/*
 * Description: Implements MIX address transfer operators. These operations use M as an
 * immediate value rather than a pointer to a MIX memory location.
 * M = signed addr indexed by i index register
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */

#include <stdbool.h>
#include <stdint.h>

#include "mixDefs.h"
#include "mixTypes.h"
#include "mixDecode.h"
#include "mix.h"

/***************************************************************************************************
*
* increment - add or subtract M to/from a register
*
*/

static void increment
(
    mixData_t   *reg,       // register to modify
    uint32_t    i,          // index register used to calculate M
    int32_t     addr,       // address used to calculate M
    uint32_t    mask,       // value used to check for overflow
    bool        negate      // true: subtract M
)
{
    int32_t regVal = mixIntTo2sC(reg);
    int32_t m = immediate(addr, i);

    m = (negate == true) ? -m : m;
    twosCToMixInt((regVal + m), reg, mask);     // twosCToMixInt() uses mask to check overflow
}

/***************************************************************************************************
*
* enter - set the value of a register to M
*
*/

static void enter
(
    mixData_t   *reg,       // register to modify
    uint32_t    i,          // index register used to calculate M
    int32_t     addr,       // address used to calculate M
    bool        negate      // true: load the negative of M
)
{
    int32_t m = immediate(addr, i);

    m = (negate == true) ? -m : m;
    twosCToMixInt(m, reg, MAGNITUDE_MASK);      // twosCToMixInt() uses mask to check overflow
}

/***************************************************************************************************
*
* imm - execute immediate operation
*
* Execute one of: INC, DEC, ENT, or ENN depending on the value of the instruction's F part.
*
*/

static status_t imm     // OK if successful, BAD_F otherwise
(
    mixData_t   *reg,   // register to modify
    uint32_t    f,      // F part used to select instruction variant
    uint32_t    i,      // index register used to calculate 
    int32_t     addr,   // address used to calculate M
    uint32_t    mask    // value used to check for overflow
)
{
    status_t result = OK;

    switch (f)
    {
        case INC_VARIANT:
            increment(reg, i, addr, mask, false);   // negate flag clear
            break;
        case DEC_VARIANT:
            increment(reg, i, addr, mask, true);    // negate flag set
            break;
        case ENT_VARIANT:
            enter(reg, i, addr, false);     // negate flag clear
            break;
        case ENN_VARIANT:
            enter(reg, i, addr, true);      // negate flag set
            break;
        default:
            result = BAD_F;
            break;
    }

    return result;
}

/***************************************************************************************************
*
* imma - execute immediate operation on the A register
*
*/

status_t imma           // OK if successful, error code otherwise, see imm()
(
    uint32_t    c,      // opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    return (imm(&A, f, i, addr, MAGNITUDE_MASK));
}

/***************************************************************************************************
*
* immx - execute immediate operation on the X register
*
*/

status_t immx           // OK if successful, error code otherwise, see imm()
(
    uint32_t    c,      // opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    return (imm(&X, f, i, addr, MAGNITUDE_MASK));
}

/***************************************************************************************************
*
* imm1 - execute immediate operation on the I1 register
*
*/

status_t imm1           // OK if successful, error code otherwise, see imm()
(
    uint32_t    c,      // opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    return (imm(&(I[1]), f, i, addr, MAX_MEMORY_CELL));
}

/***************************************************************************************************
*
* imm2 - execute immediate operation on the I2 register
*
*/

status_t imm2           // OK if successful, error code otherwise, see imm()
(
    uint32_t    c,      // opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    return (imm(&(I[2]), f, i, addr, MAX_MEMORY_CELL));
}

/***************************************************************************************************
*
* imm3 - execute immediate operation on the I3 register
*
*/

status_t imm3           // OK if successful, error code otherwise, see imm()
(
    uint32_t    c,      // opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    return (imm(&(I[3]), f, i, addr, MAX_MEMORY_CELL));
}

/***************************************************************************************************
*
* imm4 - execute immediate operation on the I4 register
*
*/

status_t imm4           // OK if successful, error code otherwise, see imm()
(
    uint32_t    c,      // opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    return (imm(&(I[4]), f, i, addr, MAX_MEMORY_CELL));
}

/***************************************************************************************************
*
* imm5 - execute immediate operation on the I5 register
*
*/

status_t imm5           // OK if successful, error code otherwise, see imm()
(
    uint32_t    c,      // opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    return (imm(&(I[5]), f, i, addr, MAX_MEMORY_CELL));
}

/***************************************************************************************************
*
* imm6 - execute immediate operation on the I6 register
*
*/

status_t imm6           // OK if successful, error code otherwise, see imm()
(
    uint32_t    c,      // opcode, unused
    uint32_t    f,      // F part
    uint32_t    i,      // index register
    int32_t     addr    // signed address
)
{
    return (imm(&(I[6]), f, i, addr, MAX_MEMORY_CELL));
}
