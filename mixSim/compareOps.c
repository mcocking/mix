/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/


/*
 * Description: Implement the MIX compare operations. These operations set the MIX L, E and G
 * flags.
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */


#include <stdio.h>

#include "mixDefs.h"
#include "mixTypes.h"
#include "mix.h"

/***************************************************************************************************
*
* compare - compare a register to the contents of the specified memory location
*
* The instruction's field specifier is applied to both the register and the memory location
* before the comparison is made.
*
*
*/

static status_t compare // OK if successful, error code otherwise
(
    mixWord_t   *reg,   // treat reg like memory for memToReg() call in order to apply F part
    uint32_t    c,      // op code, unused
    uint32_t    f,      // F part applied to both reg and memory location
    uint32_t    i,      // index register
    int32_t     addr    // signed A part
)
{
    status_t result = OK;

    operand1.bits = 0;
    result = memToReg(reg, &operand1, f, false); // negate flag clear
    if (result == OK)
    {
        uint32_t    m;
        int32_t     val1;
        int32_t     val2;

        operand2.bits = 0;
        m = memoryIndex(addr, i);

        // no need to check the result of memToReg(), f is OK

        memToReg(&(memory[m]), &operand2, f, false); // negate flag clear
        val1 = mixIntTo2sC(&operand1);
        val2 = mixIntTo2sC(&operand2);

        // set the machine's flags

        if (val1 < val2)
        {
            L = true; E = false; G = false;
        }
        else if (val1 == val2)
        {
            L = false; E = true; G = false;
        }
        else
        {
            L = false; E = false; G = true;
        }
    }

    return result;
}

/***************************************************************************************************
*
* compare - compare the A register to the contents of the specified memory location
*
* The instruction's field specifier is applied to both the register and the memory location
* before the comparison is made.
*
*
*/

status_t cmpa           // OK if successful, error code otherwise
(
    uint32_t    c,      // op code, unused
    uint32_t    f,      // F part applied to both reg and memory location
    uint32_t    i,      // index register
    int32_t     addr    // signed A part
)
{
    status_t    result;

    if (f == 6)
    {
        result = UNIMPLEMENTED_F;   // FCMP not implemented
    }
    else
    {
        mixWord_t *reg = (mixWord_t *) &(A);
        result = compare(reg, c, f, i, addr);
    }

    return result;
}

/***************************************************************************************************
*
* compare - compare the X register to the contents of the specified memory location
*
* The instruction's field specifier is applied to both the register and the memory location
* before the comparison is made.
*
*
*/

status_t cmpx           // OK if successful, error code otherwise
(
    uint32_t    c,      // op code, unused
    uint32_t    f,      // F part applied to both reg and memory location
    uint32_t    i,      // index register
    int32_t     addr    // signed A part
)
{
    mixWord_t   *reg = (mixWord_t *) &(X);
    return (compare(reg, c, f, i, addr));
}

/***************************************************************************************************
*
* compare - compare I1 register to the contents of the specified memory location
*
* The instruction's field specifier is applied to both the register and the memory location
* before the comparison is made.
*
*
*/

status_t cmp1           // OK if successful, error code otherwise
(
    uint32_t    c,      // op code, unused
    uint32_t    f,      // F part applied to both reg and memory location
    uint32_t    i,      // index register
    int32_t     addr    // signed A part
)
{
    mixWord_t   *reg = (mixWord_t *) &(I[1]);
    return (compare(reg, c, f, i, addr));
}

/***************************************************************************************************
*
* compare - compare the I2 register to the contents of the specified memory location
*
* The instruction's field specifier is applied to both the register and the memory location
* before the comparison is made.
*
*
*/

status_t cmp2           // OK if successful, error code otherwise
(
    uint32_t    c,      // op code, unused
    uint32_t    f,      // F part applied to both reg and memory location
    uint32_t    i,      // index register
    int32_t     addr    // signed A part
)
{
    mixWord_t   *reg = (mixWord_t *) &(I[2]);
    return (compare(reg, c, f, i, addr));
}

/***************************************************************************************************
*
* compare - compare the I3 register to the contents of the specified memory location
*
* The instruction's field specifier is applied to both the register and the memory location
* before the comparison is made.
*
*
*/

status_t cmp3           // OK if successful, error code otherwise
(
    uint32_t    c,      // op code, unused
    uint32_t    f,      // F part applied to both reg and memory location
    uint32_t    i,      // index register
    int32_t     addr    // signed A part
)
{
    mixWord_t   *reg = (mixWord_t *) &(I[3]);
    return (compare(reg, c, f, i, addr));
}

/***************************************************************************************************
*
* compare - compare the I4 register to the contents of the specified memory location
*
* The instruction's field specifier is applied to both the register and the memory location
* before the comparison is made.
*
*
*/

status_t cmp4           // OK if successful, error code otherwise
(
    uint32_t    c,      // op code, unused
    uint32_t    f,      // F part applied to both reg and memory location
    uint32_t    i,      // index register
    int32_t     addr    // signed A part
)
{
    mixWord_t   *reg = (mixWord_t *) &(I[4]);
    return (compare(reg, c, f, i, addr));
}

/***************************************************************************************************
*
* compare - compare the I5 register to the contents of the specified memory location
*
* The instruction's field specifier is applied to both the register and the memory location
* before the comparison is made.
*
*
*/

status_t cmp5           // OK if successful, error code otherwise
(
    uint32_t    c,      // op code, unused
    uint32_t    f,      // F part applied to both reg and memory location
    uint32_t    i,      // index register
    int32_t     addr    // signed A part
)
{
    mixWord_t   *reg = (mixWord_t *) &(I[5]);
    return (compare(reg, c, f, i, addr));
}

/***************************************************************************************************
*
* compare - compare the I6 register to the contents of the specified memory location
*
* The instruction's field specifier is applied to both the register and the memory location
* before the comparison is made.
*
*
*/

status_t cmp6           // OK if successful, error code otherwise
(
    uint32_t    c,      // op code, unused
    uint32_t    f,      // F part applied to both reg and memory location
    uint32_t    i,      // index register
    int32_t     addr    // signed A part
)
{
    mixWord_t   *reg = (mixWord_t *) &(I[6]);
    return (compare(reg, c, f, i, addr));
}


