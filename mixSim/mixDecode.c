/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/

/*
 * Description: This file contains the jump table to all the MIX operations.
 * see The Art of Computer Programming, 2nd Edition - Donald E. Knuth
 *
 */


#include "mixDefs.h"
#include "mixTypes.h"
#include "mixDecode.h"

char *statusStr[] =
{
    "OK",
    "ERROR"
    "UNIMPLEMENTED OPCODE",
    "UNIMPLEMENTED OPCODE VARIANT",
    "BAD F",
    "BAD I",
    "IO UNIT NOT ONLINE"
};

// each entry has a function pointer and instruction execution time

opTableEntry_t opTable[] =
{
    {nop,       NOP_TIME},              // 00
    {add,       ADD_SUB_TIME},          // 01
    {sub,       ADD_SUB_TIME},          // 02
    {mul,       MUL_TIME},              // 03
    {mixdiv,    DIV_TIME},              // 04 "div" conflicts with div in std lib
    {special,   SPECIAL_TIME},          // 05
    {shift,     SHIFT_TIME},            // 06
    {move,      BASE_MOVE_TIME},        // 07
    {lda,       LOAD_STORE_TIME},       // 08
    {ld1,       LOAD_STORE_TIME},       // 09
    {ld2,       LOAD_STORE_TIME},       // 10
    {ld3,       LOAD_STORE_TIME},       // 11
    {ld4,       LOAD_STORE_TIME},       // 12
    {ld5,       LOAD_STORE_TIME},       // 13
    {ld6,       LOAD_STORE_TIME},       // 14
    {ldx,       LOAD_STORE_TIME},       // 15
    {ldan,      LOAD_STORE_TIME},       // 16
    {ld1n,      LOAD_STORE_TIME},       // 17
    {ld2n,      LOAD_STORE_TIME},       // 18
    {ld3n,      LOAD_STORE_TIME},       // 19
    {ld4n,      LOAD_STORE_TIME},       // 20
    {ld5n,      LOAD_STORE_TIME},       // 21
    {ld6n,      LOAD_STORE_TIME},       // 22
    {ldxn,      LOAD_STORE_TIME},       // 23
    {sta,       LOAD_STORE_TIME},       // 24
    {st1,       LOAD_STORE_TIME},       // 25
    {st2,       LOAD_STORE_TIME},       // 26
    {st3,       LOAD_STORE_TIME},       // 27
    {st4,       LOAD_STORE_TIME},       // 28
    {st5,       LOAD_STORE_TIME},       // 29
    {st6,       LOAD_STORE_TIME},       // 30
    {stx,       LOAD_STORE_TIME},       // 31
    {stj,       LOAD_STORE_TIME},       // 32
    {stz,       LOAD_STORE_TIME},       // 33
    {jbus,      JUMP_TIME},             // 34
    {ioc,       IO_TIME},               // 35
    {in,        IO_TIME},               // 36
    {out,       IO_TIME},               // 37
    {jred,      JUMP_TIME},             // 38
    {jmp,       JUMP_TIME},             // 39
    {ja,        JUMP_TIME},             // 40
    {mixj1,     JUMP_TIME},             // 41 "j1" conflicts with j1 in math lib
    {j2,        JUMP_TIME},             // 42
    {j3,        JUMP_TIME},             // 43
    {j4,        JUMP_TIME},             // 44
    {j5,        JUMP_TIME},             // 45
    {j6,        JUMP_TIME},             // 46
    {jx,        JUMP_TIME},             // 47
    {imma,      IMMEDIATE_TIME},        // 48
    {imm1,      IMMEDIATE_TIME},        // 49
    {imm2,      IMMEDIATE_TIME},        // 50
    {imm3,      IMMEDIATE_TIME},        // 51
    {imm4,      IMMEDIATE_TIME},        // 52
    {imm5,      IMMEDIATE_TIME},        // 53
    {imm6,      IMMEDIATE_TIME},        // 54
    {immx,      IMMEDIATE_TIME},        // 55
    {cmpa,      COMPARE_TIME},          // 56
    {cmp1,      COMPARE_TIME},          // 57
    {cmp2,      COMPARE_TIME},          // 58
    {cmp3,      COMPARE_TIME},          // 59
    {cmp4,      COMPARE_TIME},          // 60
    {cmp5,      COMPARE_TIME},          // 61
    {cmp6,      COMPARE_TIME},          // 62
    {cmpx,      COMPARE_TIME}           // 63
};
