/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/


/*
 * Description: Use the terminal stop signal to stop a running MIX program. These routines are
 * OS specific.
 *
 */

#include <stdio.h>
#include <stdbool.h>
#include <signal.h>

static char *breakHelpStr = "CTRL-Z";
static volatile sig_atomic_t brk = 0;

/***************************************************************************************************
*
* sigtstpHandler - record the arrival of a terminal stop signal
*
*/

static void sigtstpHandler
(
    int sNum
)
{
    brk = 1;
}

/***************************************************************************************************
*
* getBreakHelp - return the break help string
*
*/

char *getBreakHelp(void)        // help string
{
    return breakHelpStr;
}

/***************************************************************************************************
*
* gotBreak - indicate if a terminal stop signal (break) was received
*
* For POSIX systems a terminal stop is initiated by CTRL-Z.
*
*/

bool gotBreak(void)     // true if break received, false otherwist
{
    int b = brk;
    // small chance of losing a CTRL-Z here
    brk = 0;
    return ((b == 0) ? false : true);
}

/***************************************************************************************************
*
* breakInit - set up to handle a terminal stop
*
*/

void breakInit(void)
{
    struct sigaction act;
    sigset_t set;

    sigemptyset(&set);
    act.sa_handler = sigtstpHandler;
    act.sa_mask = set;
    act.sa_flags = 0;


    if (sigaction(SIGTSTP, &act, NULL) != 0)
    {
        printf("WARNING: CTLR-Z can't be used to break running MIX program\n");
    }
    else
    {
        printf("%s to break running MIX program\n", breakHelpStr);
    }
}
