MIX Simulator, see The Art of Computer Programming, 2nd Edition - Donald E. Knuth

TAOCP - 2 example programs + a card deck from TAOCP  
include - an include file for a symbol table library  
lib - a small symbol table library  
mixSim - a MIX simulator with command line interface  
mixal - a mix assembler

The lib, assembler and MIX simulator are written in C and portable to any POSIX OS.

QUICK START

1) Compile the symbol table lib, mixal and mixSim  
2) Assemble a MIX program using mixal e.g. ./mixal myprog.mix > myprog.moct  
3) Run the assembled file e.g. ./mix myprog.moct  
4) hit "h" at the "MIX>" command prompt for help with the commands 

HINTS

1) at the "MIX>" command prompt use "u 18 filename" to mount a printer  
2) at the "MIX>" command prompt use "u 16 filename" to mount a card reader  
3) at the "MIX>" command prompt use "c" to run a loaded program
