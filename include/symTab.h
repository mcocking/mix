/*
Copyright (c) 2020 Martin Cocking

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/


/*
Trigrams
--------
m_c  Martin Cocking

Modification history
--------------------
14jul20 m_c  initial version
*/


#ifndef SYM_TAB_H
#define SYM_TAB_H

#include <stdint.h>

typedef struct symTabEntry
{
    char    *name;
    int64_t value;
} symTabEntry_t;

typedef struct symTabl
{
    symTabEntry_t   *entries;
    uint32_t        size;
} symTab_t;

extern uint32_t         symTabInit          (symTab_t *, uint32_t);
extern void             symTabFree          (symTab_t *);
extern symTabEntry_t    *lookUp             (char *, symTab_t *);
extern symTabEntry_t    *lookUpByValue      (uint32_t, symTab_t *);
extern symTabEntry_t    *lookUpNextPartial  (char *, uint32_t *, symTab_t *);
extern symTabEntry_t    *addEntry           (char *, int64_t, symTab_t *);
extern void             deleteEntry         (char *, symTab_t *);
extern void             printTable          (char *, char *, symTab_t *);

#endif // SYM_TAB_H
